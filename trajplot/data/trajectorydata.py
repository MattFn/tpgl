from __future__ import annotations
from ..TrajSeg.data import Trajectory

import numpy as np

class TrajectoryData():

    def __init__(self, trajectory:"Trajectory", annotation:"String", colors:"Dict"):
        # position data
        x = np.array([p.x for p in trajectory])
        y = np.array([p.y*-1 for p in trajectory])
        ts = np.array([p.timestamp.timestamp() for p in trajectory])

        # color data
        cl = np.array([colors[p.annotations[annotation]] for p in trajectory], dtype=np.object)
        
        r, g, b, a = np.split(cl, 4, axis=1)
        r, g, b = r.flatten(), g.flatten(), b.flatten()

        # TODO range is hardcoded
        x, y, ts = self.__normalize_values(x, y, ts, -10, 10)

        self.data = np.ndarray(shape=(x.size*6,), dtype=np.float32)

        self.data[0::6] = x
        self.data[1::6] = ts
        self.data[2::6] = y
        self.data[3::6] = r
        self.data[4::6] = g
        self.data[5::6] = b

    def get_length(self):
        return int(self.data.size / 6)
    
    def get_len_bytes(self):
        return self.data.size * self.data.itemsize
    
    def get_vertex_data(self):
        return self.data
    
    def get_x(self):
        return self.data[0::6]

    def get_y(self):
        return self.data[2::6]
    
    def get_ts(self):
        return self.data[1::6]
    
    def get_r(self):
        return self.data[3::6]

    def get_g(self):
        return self.data[4::6]

    def get_b(self):
        return self.data[5::6]

    def __normalize_values(self, x, y, ts, lower, upper):
        min_x = np.min(x)
        min_y = np.min(y)
        min_ts, max_ts = np.min(ts), np.max(ts)

        x -= min_x
        y -= min_y
        
        max_x = np.max(x)
        max_y = np.max(y)

        max_val = max(max_x, max_y)

        x = (((upper-lower)*x) / max_val) + lower
        y = (((upper-lower)*y) / max_val) + lower
        ts = (((upper-lower)*(ts - min_ts)) / (max_ts - min_ts)) + lower
        
        return x, y, ts