from .. import managers

class SettingsObject():

    def __init__(self, ts_format, bg_color, cl_colors, points_size, move_label, stop_label):
        self.ts_format = ts_format
        self.bg_color = bg_color
        self.cl_colors = cl_colors
        self.points_size = points_size
        self.move_label = move_label
        self.stop_label = stop_label

    def serialize(self):
        return {
            managers.SettingsManager.TS_FORMAT_SETTING: self.ts_format,
            managers.SettingsManager.BG_COLOR_SETTING: self.bg_color,
            managers.SettingsManager.POINTS_SIZE_SETTING: self.points_size,
            managers.SettingsManager.MOVE_LABEL_SETTING: self.move_label,
            managers.SettingsManager.STOP_LABEL_SETTING: self.stop_label,
            managers.SettingsManager.CL_COLORS_SETTING: self.cl_colors,
        }