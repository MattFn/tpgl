import sys
import numpy as np

from PyQt5.QtWidgets import (QSpacerItem, QWidget, QDialog, QSpinBox, QLineEdit, QGroupBox, QLabel, QPushButton, QColorDialog, QTabWidget, QScrollArea, QDoubleSpinBox, QProgressBar, QCheckBox)
from PyQt5.Qt import QVBoxLayout, QColor, QGridLayout, QSizePolicy
from PyQt5.QtCore import Qt, pyqtSignal, pyqtSlot, QThread

from ..TrajSeg.data import Trajectory
from ..TrajSeg.algorithms import SPD
from ..TrajSeg.algorithms import SeqScan
from ..TrajSeg.algorithms import POSMIT
from ..TrajSeg.algorithms import DBSCAN, DBSCAN_fast
from ..TrajSeg.algorithms import CBSMoT
from ..TrajSeg.algorithms import SWSPD
from ..utils import QtUtils
from ..core import AlgorithmWorker

import threading

class SegmentationDialog(QDialog):

    def __init__(self, parent, data_manager, settings_manager):
        super().__init__(parent, Qt.WindowSystemMenuHint | Qt.WindowTitleHint | Qt.WindowCloseButtonHint)
        self.parent = parent
        self.data_manager = data_manager
        self.settings_manager = settings_manager

        self.worker = None

        self.setWindowTitle("Apply segmentation")
        self.setGeometry(QtUtils.create_dialog_geom(parent.geometry(), 400, 50))
        
        self.setLayout(QVBoxLayout())

        self.tab_widget = QTabWidget()

        # algorithms tab TODO tab as widget to extend
        self.tab_widget.addTab(self.build_spd_tab(), "SPD")
        self.tab_widget.addTab(self.build_seqscan_tab(), "SeqScan")
        self.tab_widget.addTab(self.build_dbscan_tab(), "DBSCAN")
        self.tab_widget.addTab(self.build_cbsmot_tab(), "CB-SMoT")
        self.tab_widget.addTab(self.build_posmit_tab(), "POSMIT")
        self.tab_widget.addTab(self.build_swspd_tab(), "SW-SPD")

        # progress bar
        self.progress_bar = QProgressBar()
        self.progress_bar.setStyleSheet("height: 8px")
        self.progress_bar.setTextVisible(True)

        self.layout().addWidget(self.tab_widget)
        self.layout().addWidget(self.progress_bar)

    def algorithm_done(self):
        output = self.worker.get_output()
        
        if output is not None:
            name = "{}_output".format(self.worker.get_algorithm_name())
            self.data_manager.add_annotation(name, output)
            self.data_manager.select_annotation(name)
        
        self.enable_window()

    def run_algorithm(self, algorithm, params):
        algorithm.STOP_LABEL = self.settings_manager.stop_label
        algorithm.MOVE_LABEL = self.settings_manager.move_label

        self.worker = AlgorithmWorker(algorithm, params)
        self.worker.update_progress_signal.connect(self.update_progress_bar)
        self.worker.computation_done_signal.connect(self.algorithm_done)

        self.worker.start()
    
    def run_spd(self):
        self.disable_window()

        d = self.spd_dist_param.value()
        t = self.spd_time_param.value()
        cartesian = not self.spd_latlon_param.isChecked()

        trajectory = self.data_manager.get_trajectory()
        spd = SPD(trajectory, silent=False)
        self.run_algorithm(spd, {"d_thres": d, "t_thres": t})

    def run_ss(self):
        self.disable_window()

        eps = self.ss_eps_param.value()
        presence = self.ss_presence_param.value()
        n = self.ss_n_param.value()
        cartesian = not self.ss_latlon_param.isChecked()

        trajectory = self.data_manager.get_trajectory()
        seqscan = SeqScan(trajectory, silent=False)
        self.run_algorithm(seqscan, {"distance": eps, "n_points": n, "presence": presence})

    def run_DBSCAN(self):
        self.disable_window()

        eps = self.dbs_eps_param.value()
        n = self.dbs_n_param.value()
        cartesian = not self.dbs_latlon_param.isChecked()

        trajectory = self.data_manager.get_trajectory()
        dbs = DBSCAN_fast(trajectory)
        self.run_algorithm(dbs, {"eps": eps, "min_points": n})

    def run_cbsmot(self):
        self.disable_window()

        eps = self.cb_eps_param.value()
        min_time = self.cb_min_time_param.value()
        cartesian = not self.cb_latlon_param.isChecked()

        trajectory = self.data_manager.get_trajectory()
        cbsmot = CBSMoT(trajectory, silent=False)
        self.run_algorithm(cbsmot, {"eps":eps, "min_time":min_time})

    def run_posmit(self):
        self.disable_window()

        sr = None if self.pos_estimate_sr.isChecked() else self.pos_search_radius.value()
        sv = None if self.pos_estimate_sv.isChecked() else self.pos_stop_variance.value()
        msp = None if self.pos_estimate_msp.isChecked() else self.pos_min_stop_pr.value()
        cartesian = not self.cb_latlon_param.isChecked()

        trajectory = self.data_manager.get_trajectory()
        posmit = POSMIT(trajectory, silent=False)
        self.run_algorithm(posmit, {"nSearchRadius": sr, "stopVariance": sv, "minStopConfidence": msp})

    def run_swspd(self):
        self.disable_window()

        d = self.swspd_dist_param.value()
        t = self.swspd_time_param.value()
        cartesian = not self.swspd_latlon_param.isChecked()

        trajectory = self.data_manager.get_trajectory()
        swspd = SWSPD(trajectory, silent=False)
        self.run_algorithm(swspd, {"distance_threshold": d, "time_threshold": t})

    @pyqtSlot(int)
    def update_progress_bar(self, value):
        self.progress_bar.setValue(value)

    def disable_window(self):
        self.tab_widget.setDisabled(True)

    def enable_window(self):
        self.tab_widget.setDisabled(False)

    def reject(self):
        if self.worker is not None:
            self.worker.terminate()
            self.worker.wait()

        super().reject()

    # GUI building
    def build_spd_tab(self):
        spd_tab = QWidget()
        spd_tab.setLayout(QVBoxLayout())

        spd_params_box = QGroupBox("Params")
        spd_params_box.setLayout(QGridLayout())

        self.spd_dist_param = QDoubleSpinBox()
        self.spd_dist_param.setMinimum(0)
        self.spd_dist_param.setMaximum(sys.float_info.max)
        self.spd_dist_param.setSingleStep(0.1)

        self.spd_time_param = QDoubleSpinBox()
        self.spd_time_param.setMinimum(0)
        self.spd_time_param.setMaximum(sys.float_info.max)
        self.spd_time_param.setSingleStep(1)

        self.spd_latlon_param = QCheckBox("use latlon")
        self.spd_latlon_param.setDisabled(True)
        
        spd_params_box.layout().addWidget(QLabel("distance:"), 0, 0, Qt.AlignTop)
        spd_params_box.layout().addWidget(self.spd_dist_param, 0, 1, Qt.AlignTop)
        spd_params_box.layout().addWidget(QLabel("time:"), 1, 0, Qt.AlignTop)
        spd_params_box.layout().addWidget(self.spd_time_param, 1, 1, Qt.AlignTop)
        spd_params_box.layout().addWidget(self.spd_latlon_param, 2, 0, Qt.AlignTop)
        spd_params_box.layout().addItem(QSpacerItem(5, 5, QSizePolicy.Minimum, QSizePolicy.Expanding), 3, 0, 1, 1, Qt.AlignTop)

        spd_button = QPushButton("apply")
        spd_button.clicked.connect(self.run_spd)

        spd_tab.layout().addWidget(spd_params_box)
        spd_tab.layout().addWidget(spd_button)

        return spd_tab

    def build_seqscan_tab(self):
        ss_tab = QWidget()
        ss_tab.setLayout(QVBoxLayout())

        ss_params_box = QGroupBox("Params")
        ss_params_box.setLayout(QGridLayout())

        self.ss_eps_param = QDoubleSpinBox()
        self.ss_eps_param.setMinimum(0)
        self.ss_eps_param.setMaximum(sys.float_info.max)
        self.ss_eps_param.setSingleStep(0.1)

        self.ss_presence_param = QDoubleSpinBox()
        self.ss_presence_param.setDecimals(3)
        self.ss_presence_param.setMinimum(0)
        self.ss_presence_param.setMaximum(sys.float_info.max)
        self.ss_presence_param.setSingleStep(1)

        self.ss_n_param = QDoubleSpinBox()
        self.ss_n_param.setMinimum(0)
        self.ss_n_param.setMaximum(sys.float_info.max)
        self.ss_n_param.setSingleStep(1)

        self.ss_latlon_param = QCheckBox("use latlon")
        self.ss_latlon_param.setDisabled(True)
        
        ss_params_box.layout().addWidget(QLabel("eps (m):"), 0, 0, Qt.AlignTop)
        ss_params_box.layout().addWidget(self.ss_eps_param, 0, 1, Qt.AlignTop)
        ss_params_box.layout().addWidget(QLabel("presence (s):"), 1, 0, Qt.AlignTop)
        ss_params_box.layout().addWidget(self.ss_presence_param, 1, 1, Qt.AlignTop)
        ss_params_box.layout().addWidget(QLabel("n points:"), 2, 0, Qt.AlignTop)
        ss_params_box.layout().addWidget(self.ss_n_param, 2, 1, Qt.AlignTop)
        ss_params_box.layout().addWidget(self.ss_latlon_param, 3, 0, Qt.AlignTop)
        ss_params_box.layout().addItem(QSpacerItem(5, 5, QSizePolicy.Minimum, QSizePolicy.Expanding), 4, 0, 1, 1, Qt.AlignTop)

        ss_button = QPushButton("apply")
        ss_button.clicked.connect(self.run_ss)

        ss_tab.layout().addWidget(ss_params_box)
        ss_tab.layout().addWidget(ss_button)

        return ss_tab

    def build_dbscan_tab(self):
        dbs_tab = QWidget()
        dbs_tab.setLayout(QVBoxLayout())

        dbs_params_box = QGroupBox("Params")
        dbs_params_box.setLayout(QGridLayout())
 
        self.dbs_eps_param = QDoubleSpinBox()
        self.dbs_eps_param.setDecimals(8)
        self.dbs_eps_param.setMinimum(0)
        self.dbs_eps_param.setMaximum(sys.float_info.max)
        self.dbs_eps_param.setSingleStep(0.1)

        self.dbs_n_param = QDoubleSpinBox()
        self.dbs_n_param.setMinimum(0)
        self.dbs_n_param.setMaximum(sys.float_info.max)
        self.dbs_n_param.setSingleStep(1)

        self.dbs_latlon_param = QCheckBox("use latlon")
        self.dbs_latlon_param.setDisabled(True)

        dbs_params_box.layout().addWidget(QLabel("eps (m):"), 0, 0, Qt.AlignTop)
        dbs_params_box.layout().addWidget(self.dbs_eps_param, 0, 1, Qt.AlignTop)
        dbs_params_box.layout().addWidget(QLabel("n points:"), 1, 0, Qt.AlignTop)
        dbs_params_box.layout().addWidget(self.dbs_n_param, 1, 1, Qt.AlignTop)
        dbs_params_box.layout().addWidget(self.dbs_latlon_param, 2, 0, Qt.AlignTop)
        dbs_params_box.layout().addItem(QSpacerItem(5, 5, QSizePolicy.Minimum, QSizePolicy.Expanding), 3, 0, 1, 1, Qt.AlignTop)

        self.dbs_button = QPushButton("apply")
        self.dbs_button.clicked.connect(self.run_DBSCAN)

        dbs_tab.layout().addWidget(dbs_params_box)
        dbs_tab.layout().addWidget(self.dbs_button)

        return dbs_tab

    def build_cbsmot_tab(self):
        cb_tab = QWidget()
        cb_tab.setLayout(QVBoxLayout())

        cb_params_box = QGroupBox("Params")
        cb_params_box.setLayout(QGridLayout())

        self.cb_eps_param = QDoubleSpinBox()
        self.cb_eps_param.setMinimum(0)
        self.cb_eps_param.setMaximum(sys.float_info.max)
        self.cb_eps_param.setSingleStep(0.1)

        self.cb_min_time_param = QDoubleSpinBox()
        self.cb_min_time_param.setDecimals(3)
        self.cb_min_time_param.setMinimum(0)
        self.cb_min_time_param.setMaximum(sys.float_info.max)
        self.cb_min_time_param.setSingleStep(1)

        self.cb_latlon_param = QCheckBox("use latlon")
        self.cb_latlon_param.setDisabled(True)
        
        cb_params_box.layout().addWidget(QLabel("eps (m):"), 0, 0, Qt.AlignTop)
        cb_params_box.layout().addWidget(self.cb_eps_param, 0, 1, Qt.AlignTop)
        cb_params_box.layout().addWidget(QLabel("min time (s):"), 1, 0, Qt.AlignTop)
        cb_params_box.layout().addWidget(self.cb_min_time_param, 1, 1, Qt.AlignTop)
        cb_params_box.layout().addWidget(self.cb_latlon_param, 2, 0, Qt.AlignTop)
        cb_params_box.layout().addItem(QSpacerItem(5, 5, QSizePolicy.Minimum, QSizePolicy.Expanding), 3, 0, 1, 1, Qt.AlignTop)

        cb_button = QPushButton("apply")
        cb_button.clicked.connect(self.run_cbsmot)

        cb_tab.layout().addWidget(cb_params_box)
        cb_tab.layout().addWidget(cb_button)

        return cb_tab
    
    def build_posmit_tab(self):
        pos_tab = QWidget()
        pos_tab.setLayout(QVBoxLayout())

        pos_params_box = QGroupBox("Params")
        pos_params_box.setLayout(QGridLayout())

        self.pos_search_radius = QSpinBox()
        self.pos_search_radius.setMinimum(0)
        self.pos_search_radius.setMaximum(9999999)

        self.pos_estimate_sr = QCheckBox("Estimate search radius")
        self.pos_estimate_sr.toggled.connect(lambda b : self.pos_search_radius.setDisabled(True) if b else self.pos_search_radius.setDisabled(False))

        self.pos_stop_variance = QDoubleSpinBox()
        self.pos_stop_variance.setMinimum(0)
        self.pos_stop_variance.setMaximum(sys.float_info.max)
        self.pos_stop_variance.setSingleStep(0.1)

        self.pos_estimate_sv = QCheckBox("Estimate stop variance")
        self.pos_estimate_sv.toggled.connect(lambda b : self.pos_stop_variance.setDisabled(True) if b else self.pos_stop_variance.setDisabled(False))

        self.pos_min_stop_pr = QDoubleSpinBox()
        self.pos_min_stop_pr.setMinimum(0)
        self.pos_min_stop_pr.setMaximum(sys.float_info.max)
        self.pos_min_stop_pr.setSingleStep(0.1)

        self.pos_estimate_msp = QCheckBox("Estimate min stop probability")
        self.pos_estimate_msp.toggled.connect(lambda b : self.pos_min_stop_pr.setDisabled(True) if b else self.pos_min_stop_pr.setDisabled(False))

        self.pos_latlon_param = QCheckBox("use latlon")
        self.pos_latlon_param.setDisabled(True)
        
        pos_params_box.layout().addWidget(QLabel("search radius:"), 0, 0, Qt.AlignTop)
        pos_params_box.layout().addWidget(self.pos_search_radius, 0, 1, Qt.AlignTop)
        pos_params_box.layout().addWidget(self.pos_estimate_sr, 1, 0, Qt.AlignTop)

        pos_params_box.layout().addWidget(QLabel("stop variance:"), 2, 0, Qt.AlignTop)
        pos_params_box.layout().addWidget(self.pos_stop_variance, 2, 1, Qt.AlignTop)
        pos_params_box.layout().addWidget(self.pos_estimate_sv, 3, 0, Qt.AlignTop)

        pos_params_box.layout().addWidget(QLabel("min stop pr:"), 4, 0, Qt.AlignTop)
        pos_params_box.layout().addWidget(self.pos_min_stop_pr, 4, 1, Qt.AlignTop)
        pos_params_box.layout().addWidget(self.pos_estimate_msp, 5, 0, Qt.AlignTop)

        pos_params_box.layout().addWidget(self.pos_latlon_param, 6, 0, Qt.AlignTop)
        pos_params_box.layout().addItem(QSpacerItem(5, 5, QSizePolicy.Minimum, QSizePolicy.Expanding), 7, 0, 1, 1, Qt.AlignTop)

        pos_button = QPushButton("apply")
        pos_button.clicked.connect(self.run_posmit)

        pos_tab.layout().addWidget(pos_params_box)
        pos_tab.layout().addWidget(pos_button)

        return pos_tab

    def build_swspd_tab(self):
        swspd_tab = QWidget()
        swspd_tab.setLayout(QVBoxLayout())

        swspd_params_box = QGroupBox("Params")
        swspd_params_box.setLayout(QGridLayout())

        self.swspd_dist_param = QDoubleSpinBox()
        self.swspd_dist_param.setMinimum(0)
        self.swspd_dist_param.setMaximum(sys.float_info.max)
        self.swspd_dist_param.setSingleStep(0.1)

        self.swspd_time_param = QDoubleSpinBox()
        self.swspd_time_param.setMinimum(0)
        self.swspd_time_param.setMaximum(sys.float_info.max)
        self.swspd_time_param.setSingleStep(1)

        self.swspd_latlon_param = QCheckBox("use latlon")
        self.swspd_latlon_param.setDisabled(True)
        
        swspd_params_box.layout().addWidget(QLabel("distance:"), 0, 0, Qt.AlignTop)
        swspd_params_box.layout().addWidget(self.swspd_dist_param, 0, 1, Qt.AlignTop)
        swspd_params_box.layout().addWidget(QLabel("time:"), 1, 0, Qt.AlignTop)
        swspd_params_box.layout().addWidget(self.swspd_time_param, 1, 1, Qt.AlignTop)
        swspd_params_box.layout().addWidget(self.swspd_latlon_param, 2, 0, Qt.AlignTop)
        swspd_params_box.layout().addItem(QSpacerItem(5, 5, QSizePolicy.Minimum, QSizePolicy.Expanding), 3, 0, 1, 1, Qt.AlignTop)

        swspd_button = QPushButton("apply")
        swspd_button.clicked.connect(self.run_swspd)

        swspd_tab.layout().addWidget(swspd_params_box)
        swspd_tab.layout().addWidget(swspd_button)

        return swspd_tab
