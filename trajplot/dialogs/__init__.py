from .openfile import OpenFileDialog
from .settings import SettingsDialog
from .segmentationdialog import SegmentationDialog
from .evaluatedialog import EvaluateDialog