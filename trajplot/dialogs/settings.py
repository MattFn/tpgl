from ..managers import DataManager, SettingsManager
from ..widgets import LegendWidget
from ..utils import QtUtils
from ..data import SettingsObject

from PyQt5.QtWidgets import (QWidget, QDialog, QLineEdit, QGroupBox, QLabel, QPushButton, QColorDialog, QTabWidget, QScrollArea, QSpinBox)
from PyQt5.Qt import QVBoxLayout, QColor, QGridLayout
from PyQt5.QtCore import Qt

class SettingsDialog(QDialog):

    def __init__(self, parent, settings:SettingsManager, data:DataManager):
        super(SettingsDialog, self).__init__(parent, Qt.WindowSystemMenuHint | Qt.WindowTitleHint | Qt.WindowCloseButtonHint)
        self.parent = parent
        self.settings = settings
        self.data = data
        self.setWindowTitle("Settings")
        self.setGeometry(QtUtils.create_dialog_geom(parent.geometry(), 260, 160))

        self.setLayout(QVBoxLayout())

        # tab widget
        tab_widget = QTabWidget()

        # data tab
        data_tab = QWidget()
        data_tab.setLayout(QVBoxLayout())

        group_box1 = QGroupBox("Default timestamp format")
        group_box1.setLayout(QVBoxLayout())

        self.ts_format_input = QLineEdit()
        self.ts_format_input.setText(self.settings.ts_format)
        group_box1.layout().addWidget(self.ts_format_input)

        group_box2 = QGroupBox("STOP/MOVE")
        group_box2.setLayout(QGridLayout())

        move_label = QLabel("move label: ")
        self.move_input = QLineEdit()
        self.move_input.setText(self.settings.move_label)

        stop_label = QLabel("stop label: ")
        self.stop_input = QLineEdit()
        self.stop_input.setText(self.settings.stop_label)

        group_box2.layout().addWidget(stop_label, 0, 0, 1, 1)
        group_box2.layout().addWidget(self.stop_input, 0, 1, 1, 1)
        group_box2.layout().addWidget(move_label, 1, 0, 1, 1)
        group_box2.layout().addWidget(self.move_input, 1, 1, 1, 1)

        data_tab.layout().addWidget(group_box1)
        data_tab.layout().addWidget(group_box2)
        data_tab.layout().addStretch(1)

        # color tab
        color_tab = QWidget()
        color_tab.setLayout(QVBoxLayout())

        group_box2_5 = QGroupBox("Points size")
        group_box2_5.setLayout(QVBoxLayout())
        self.points_size_input = QSpinBox()
        self.points_size_input.setValue(self.settings.points_size)
        group_box2_5.layout().addWidget(self.points_size_input)
        color_tab.layout().addWidget(group_box2_5)

        group_box3 = QGroupBox("Background color")
        group_box3.setLayout(QVBoxLayout())

        self.bg_color = QColor()
        self.bg_color.setRgbF(*self.settings.bg_color)

        self.bg_color_btn = QPushButton()
        self.bg_color_btn.setStyleSheet('background: rgb({}, {}, {})'.format(self.bg_color.red(), self.bg_color.green(), self.bg_color.blue()))
        self.bg_color_btn.clicked.connect(self.open_color_picker)
        
        group_box3.layout().addWidget(self.bg_color_btn)
        color_tab.layout().addWidget(group_box3)

        # color tab - cls
        group_box4 = QGroupBox("Class colors")
        group_box4.setLayout(QGridLayout())

        self.legend = LegendWidget(self.data, self.settings, editable=True)
        group_box4.layout().addWidget(self.legend, 0, 0, 1, 2)
        color_tab.layout().addWidget(group_box4)
        
        tab_widget.addTab(data_tab, "Data")
        tab_widget.addTab(color_tab, "View")

        # apply button
        button = QPushButton("apply")
        button.clicked.connect(self.save_settings)

        self.layout().addWidget(tab_widget)
        self.layout().addWidget(button)

    def open_color_picker(self):
        color = QColorDialog.getColor()
        if color.isValid():
            self.bg_color = color
            self.bg_color_btn.setStyleSheet('background: rgb({}, {}, {})'.format(color.red(), color.green(), color.blue()))
    
    def save_settings(self):
        settings = SettingsObject( 
            ts_format = self.ts_format_input.text(),
            bg_color = self.bg_color.getRgbF(),
            cl_colors = self.legend.get_colors(),
            points_size = self.points_size_input.value(),
            move_label = self.move_input.text(),
            stop_label = self.stop_input.text()
        )

        self.settings.set_settings(settings)
        self.done(1)