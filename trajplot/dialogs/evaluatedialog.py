from __future__ import annotations

from csv import DictWriter
import os
import numpy as np
from PyQt5.QtWidgets import (QWidget, QDialog, QLineEdit, QGroupBox, QLabel, QPushButton, QColorDialog, QTabWidget, QScrollArea, QSpinBox, QComboBox)
from PyQt5.Qt import QVBoxLayout, QColor, QGridLayout
from PyQt5.QtCore import Qt, pyqtSignal, QObject

from ..utils import QtUtils
from ..managers import DataManager
from ..TrajSeg import evaluate

class EvaluateDialog(QDialog):

    def __init__(self, parent, data_manager:DataManager, export_path):
        super(EvaluateDialog, self).__init__(parent, Qt.WindowSystemMenuHint | Qt.WindowTitleHint | Qt.WindowCloseButtonHint)
        self.parent = parent
        self.data_manager = data_manager
        self.export_path = export_path

        self.setWindowTitle("Evaluate")
        QtUtils.create_dialog_geom(parent.geometry(), 300, 200)

        self.trajectory = self.data_manager.get_trajectory()
        
        self.__build_GUI()

        # populate boxes
        fields = self.trajectory.get_annotations()
        self.gt_dropdown.addItems(fields)
        self.res_dropdown.addItems(fields)
        

    def evaluate(self):
        gt_cl = self.gt_dropdown.currentText()
        res_cl = self.res_dropdown.currentText()

        move_cl = self.move_class.text()
        
        gt_seg = [p.annotations[gt_cl] for p in self.trajectory]
        res_seg = [p.annotations[res_cl] for p in self.trajectory]

        mcc = evaluate.compute_MCC(gt_seg, res_seg, move_cl)
        fscore = evaluate.compute_FScore(gt_seg, res_seg, move_cl)
        ari = evaluate.compute_ari(gt_seg, res_seg)

        gt_stops = self.trajectory.group_by_annotation(gt_cl, exclude=[move_cl])
        res_stops = self.trajectory.group_by_annotation(res_cl, exclude=[move_cl])

        self.stop_coverageBM_data = evaluate.compute_stop_coverage_best_match(gt_stops, res_stops)
        stop_coverageBM = sum(item['OverlapScore'] for item in self.stop_coverageBM_data) / len(self.stop_coverageBM_data)

        self.stop_coverageAM_data = evaluate.compute_stop_coverage_average_match(gt_stops, res_stops)
        stop_coverageAM = sum(item['OverlapScore'] for item in self.stop_coverageAM_data) / len(self.stop_coverageAM_data)


        self.mcc_label.setText("{}".format(mcc))
        self.ari_label.setText("{}".format(ari))
        self.fscore_label.setText("{}".format(fscore))
        self.stop_coverageBM_label.setText("{}".format(stop_coverageBM))
        self.stop_coverageAM_label.setText("{}".format(stop_coverageAM))

        self.export_stop_coverageBM_btn.setDisabled(False)
        self.export_stop_coverageAM_btn.setDisabled(False)

    def __build_GUI(self):
        self.setLayout(QVBoxLayout())

        grid = QWidget()
        grid.setLayout(QGridLayout())

        self.gt_dropdown = QComboBox()
        self.res_dropdown = QComboBox() 

        self.mcc_label = QLabel("-")
        self.ari_label = QLabel("-")
        self.fscore_label = QLabel("-")
        self.stop_coverageBM_label = QLabel("-")
        self.stop_coverageAM_label = QLabel("-")

        self.move_class = QLineEdit()

        self.eval_btn = QPushButton("evaluate")
        self.eval_btn.clicked.connect(self.evaluate)

        self.export_stop_coverageBM_btn = QPushButton("csv")
        self.export_stop_coverageBM_btn.clicked.connect(lambda : self.export_result(0))
        self.export_stop_coverageBM_btn.setDisabled(True)

        self.export_stop_coverageAM_btn = QPushButton("csv")
        self.export_stop_coverageAM_btn.clicked.connect(lambda : self.export_result(1))
        self.export_stop_coverageAM_btn.setDisabled(True)

        grid.layout().addWidget(QLabel("Ground truth annotation"),  0, 0, 1, 1)
        grid.layout().addWidget(self.gt_dropdown,                   0, 1, 1, 2)

        grid.layout().addWidget(QLabel("Result annotation"),        1, 0, 1, 1)
        grid.layout().addWidget(self.res_dropdown,                  1, 1, 1, 2)
        grid.layout().addWidget(QLabel("MOVE class"),               2, 0, 1, 1)
        grid.layout().addWidget(self.move_class,                    2, 1, 1, 2)
        
        grid.layout().addWidget(QLabel("MCC"),                      3, 0, 1, 1)
        grid.layout().addWidget(self.mcc_label,                     3, 1, 1, 1)

        grid.layout().addWidget(QLabel("ARI"),                      4, 0, 1, 1)
        grid.layout().addWidget(self.ari_label,                     4, 1, 1, 1)

        grid.layout().addWidget(QLabel("F-score"),                  5, 0, 1, 1)
        grid.layout().addWidget(self.fscore_label,                  5, 1, 1, 1)

        grid.layout().addWidget(QLabel("Stop coverage(BM)"),        6, 0, 1, 1)
        grid.layout().addWidget(self.stop_coverageBM_label,         6, 1, 1, 1)
        grid.layout().addWidget(self.export_stop_coverageBM_btn,    6, 2, 1, 1)

        grid.layout().addWidget(QLabel("Stop coverage(AM)"),        7, 0, 1, 1)
        grid.layout().addWidget(self.stop_coverageAM_label,         7, 1, 1, 1)
        grid.layout().addWidget(self.export_stop_coverageAM_btn,    7, 2, 1, 1)
        
        self.layout().addWidget(grid)
        self.layout().addStretch()

        self.layout().addWidget(self.eval_btn)

    def export_result(self, select=0):
        to_export = None
        if select == 0:
            to_export = self.stop_coverageBM_data
            output_name = QtUtils.create_output_filename(self.export_path, "stop_coverageBM_" + self.data_manager.file_name, ".csv")
        elif select == 1:
            to_export = self.stop_coverageAM_data
            output_name = QtUtils.create_output_filename(self.export_path, "stop_coverageAM_" + self.data_manager.file_name, ".csv")

        with open(output_name, "w") as f:
            r = DictWriter(f, fieldnames=to_export[0].keys())
            r.writeheader()
            r.writerows(to_export)

        QtUtils.display_message("Export complete", "exported to:\n{}".format(self.export_path), 2)
        
