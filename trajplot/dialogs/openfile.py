from __future__ import annotations

from os import path
from csv import DictReader
from datetime import datetime
import numpy as np

from PyQt5.QtWidgets import (QCheckBox, QWidget, QDialog, QLineEdit, QGroupBox, QLabel, QPushButton, QComboBox, QFileDialog, QListWidget, QMessageBox)
from PyQt5.Qt import QVBoxLayout, QGridLayout, QHBoxLayout
from PyQt5.QtCore import Qt, pyqtSignal, QObject

from ..utils import QtUtils, Utils

class OpenFileDialog(QDialog):

    CLASS_CARDINALITY_WARNING_THRESHOLD = 100

    LAT_HINTS = ["LAT", "Y"]
    LON_HINTS = ["LON", "X"]
    TS_HINTS = ["TS", "TIME"]

    def __init__(self, parent, data_manager, settings):
        super(OpenFileDialog, self).__init__(parent, Qt.WindowSystemMenuHint | Qt.WindowTitleHint | Qt.WindowCloseButtonHint)
        self.parent = parent
        self.data_manager = data_manager
        self.settings = settings

        self.setWindowTitle("Open file")
        self.setGeometry(QtUtils.create_dialog_geom(parent.geometry(), 500, 500))

        self.__build_GUI()

        self.is_file_open = False
        self.file_path = None
        self.file_contents = None
        
    def confirm_selection(self):
        if not self.is_file_open:
            return

        x = self.cbox_x.currentText()
        y = self.cbox_y.currentText()
        ts = self.cbox_time.currentText()
        ts_format = self.ledit_date_format.text()
        latlon = self.latlon_checkbox.isChecked()

        if self.annotations_list.count() != 0:
            annotations = [self.annotations_list.item(i).text() for i in range(self.annotations_list.count())]
        else:
            annotations = None

        self.data_manager.load_data(self.file_contents, self.file_path, x, y, ts, ts_format, annotations, latlon)
        self.accept()

    def discard_selection(self):
        self.reject()

    def open_file_selection(self):
        location = self.data_manager.last_path

        file_name = QFileDialog.getOpenFileName(self, 'Open file', location, "CSV files (*.csv)")[0]
        if len(file_name) == 0:
            return
        
        self.data_manager.last_path = file_name
        self.ledit_path.setText(file_name)

        self.clear_widgets()

        self.parse_file(file_name)   

    def parse_file(self, path):
        with open(path) as f:
            fl = list(DictReader(f))

        self.file_contents = fl
        self.file_path = path
        self.is_file_open = True

        # populate fields
        fields = fl[0].keys()
        self.cbox_x.addItems(fields)
        self.cbox_y.addItems(fields)
        self.cbox_time.addItems(fields)
        self.cbox_annotations.addItems(fields)

        # guess fields
        for i, f in enumerate(fields):
            if any(v in f.upper() for v in self.LON_HINTS):
                self.cbox_x.setCurrentIndex(i)
            if any(v in f.upper() for v in self.LAT_HINTS):
                self.cbox_y.setCurrentIndex(i)
            if any(v in f.upper() for v in self.TS_HINTS):
                self.cbox_time.setCurrentIndex(i)
        
        if (frmt := Utils.guess_date_format(fl[0][self.cbox_time.currentText()])) is not None:
            self.ledit_date_format.setText(frmt)
        else:
            self.ledit_date_format.setText(self.settings.ts_format)

    def add_annotation(self):
        if not self.is_file_open:
            return

        selected_annotation = self.cbox_annotations.currentText()
        items = [self.annotations_list.item(i).text() for i in range(self.annotations_list.count())]
        
        # check if the annotation is already selected
        if selected_annotation in items:
            return

        # check number of classes
        n = len(np.unique([v[selected_annotation] for v in self.file_contents]))
        if n > OpenFileDialog.CLASS_CARDINALITY_WARNING_THRESHOLD:
            reply = QtUtils.display_question("Warning", "Adding {} as an annotation will generate {} classes, this might not be intended.\n Do you still wish to add it?".format(selected_annotation, n))
            if reply == QMessageBox.Cancel:
                return

        self.annotations_list.addItem(selected_annotation)

    def remove_annotation(self):
        if not self.is_file_open:
            return

        selected_row = self.annotations_list.currentRow()
        self.annotations_list.takeItem(selected_row)

    def clear_widgets(self):
        self.cbox_x.clear()
        self.cbox_y.clear()
        self.cbox_time.clear()
        self.cbox_annotations.clear()
        self.annotations_list.clear()

    def validate_format(self):
        if not self.is_file_open:
            self.lbl_format_validation.setText("...")
            return

        ts = self.cbox_time.currentText()
        ts_format = self.ledit_date_format.text()

        if not ts or not ts_format:
            self.lbl_format_validation.setText("...")
            return

        date = self.file_contents[0][ts]

        try:
            datetime.strptime(date, ts_format)
            self.lbl_format_validation.setText("valid format")
        except:
            self.lbl_format_validation.setText("invalid format")

    def __build_GUI(self):
        self.setLayout(QVBoxLayout())
        self.layout().setAlignment(Qt.AlignTop)

        self.btn_open_file = QPushButton("open")
        self.btn_cancel = QPushButton("cancel")
        self.btn_confirm = QPushButton("apply")
        self.btn_add_annotation = QPushButton("add")
        self.btn_remove_annotation = QPushButton("remove")

        self.ledit_path = QLineEdit()
        self.ledit_date_format = QLineEdit(self.settings.ts_format)

        self.cbox_x = QComboBox()
        self.cbox_y = QComboBox()
        self.cbox_time = QComboBox()
        self.cbox_annotations = QComboBox()

        self.latlon_checkbox = QCheckBox("lat/lon")

        self.annotations_list = QListWidget()
        self.annotations_list.setStyleSheet("alternate-background-color: #dadada;")

        self.lbl_format_validation = QLabel()

        open_widget = QWidget()
        open_widget.setLayout(QHBoxLayout())
        open_widget.layout().addWidget(self.ledit_path)
        open_widget.layout().addWidget(self.btn_open_file)

        self.layout().addWidget(open_widget)

        fields_box = QGroupBox("Fields")
        fields_box.setLayout(QGridLayout())
        fields_box.layout().addWidget(QLabel("x: "),                0, 0, 1, 1)
        fields_box.layout().addWidget(self.cbox_x,                  0, 1, 1, 3)
        fields_box.layout().addWidget(QLabel("y: "),                1, 0, 1, 1)
        fields_box.layout().addWidget(self.cbox_y,                  1, 1, 1, 3)
        fields_box.layout().addWidget(QLabel("time: "),             2, 0, 1, 1)
        fields_box.layout().addWidget(self.cbox_time,               2, 1, 1, 3)
        fields_box.layout().addWidget(QLabel("time format: "),      3, 0, 1, 1)
        fields_box.layout().addWidget(self.ledit_date_format,       3, 1, 1, 2)
        fields_box.layout().addWidget(self.lbl_format_validation,   3, 3, 1, 1)
        fields_box.layout().addWidget(self.latlon_checkbox,         4, 0, 1, 2)


        self.layout().addWidget(fields_box)

        annotations_box = QGroupBox("Annotations")
        annotations_box.setLayout(QGridLayout())
        annotations_box.layout().addWidget(self.annotations_list,       0, 0, 3, 3)
        annotations_box.layout().addWidget(self.cbox_annotations,       0, 4, 1, 2)
        annotations_box.layout().addWidget(self.btn_add_annotation,     1, 4, 1, 1)
        annotations_box.layout().addWidget(self.btn_remove_annotation,  1, 5, 1, 1)

        self.layout().addWidget(annotations_box)
        self.layout().addStretch()

        btns_widget = QWidget()
        btns_widget.setLayout(QHBoxLayout())
        btns_widget.layout().setAlignment(Qt.AlignRight)

        btns_widget.layout().addWidget(self.btn_cancel)
        btns_widget.layout().addWidget(self.btn_confirm)

        self.layout().addWidget(btns_widget)

        #actions
        self.btn_open_file.clicked.connect(self.open_file_selection)
        self.btn_confirm.clicked.connect(self.confirm_selection)
        self.btn_cancel.clicked.connect(self.discard_selection)
        self.btn_add_annotation.clicked.connect(self.add_annotation)
        self.btn_remove_annotation.clicked.connect(self.remove_annotation)

        self.ledit_date_format.textChanged.connect(self.validate_format)
        self.cbox_time.currentIndexChanged.connect(self.validate_format)