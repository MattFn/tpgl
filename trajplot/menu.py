from PyQt5.QtWidgets import QAction, QMenuBar

class Menu(QMenuBar):

    def __init__(self, parent, mainwindow):
        super().__init__(parent)
        self.mainwindow = mainwindow

        self.__build_actions()
        self.__build_menus()

    def __build_actions(self):
        separator = QAction(self)
        separator.setSeparator(True)

        self.exit_action = QAction('Exit', self)
        self.exit_action.setShortcut('Ctrl+Q')
        self.exit_action.setStatusTip('Exit application')
        self.exit_action.triggered.connect(self.mainwindow.quit)

        self.open_file_action = QAction('Open File', self)
        self.open_file_action.setShortcut('Ctrl+O')
        self.open_file_action.setStatusTip('Load a trajectory')
        self.open_file_action.triggered.connect(self.mainwindow.open_file)

        self.settings_action = QAction('Settings', self)
        self.settings_action.setShortcut('Ctrl+P')
        self.settings_action.setStatusTip('Settings')
        self.settings_action.triggered.connect(self.mainwindow.open_settings)

        self.segmentation_action = QAction('Segmentation', self)
        self.segmentation_action.setShortcut('Ctrl+1')
        self.segmentation_action.setStatusTip('Apply one of the avaiable segmentation algorithms to the trajectory')
        self.segmentation_action.triggered.connect(self.mainwindow.open_segmentation)

        self.toggle_legend_action = QAction('Toggle legend', self)
        self.toggle_legend_action.setStatusTip('Show or hide the legend')
        self.toggle_legend_action.setCheckable(True)
        self.toggle_legend_action.setChecked(True)
        self.toggle_legend_action.triggered.connect(self.mainwindow.toggle_legend)

        self.toggle_info_action = QAction('Toggle info', self)
        self.toggle_info_action.setStatusTip('Show or hide the informations')
        self.toggle_info_action.setCheckable(True)
        self.toggle_info_action.setChecked(True)
        self.toggle_info_action.triggered.connect(self.mainwindow.toggle_info)

        self.cycle_annotations_action = QAction('Cycle annotations', self)
        self.cycle_annotations_action.setStatusTip('Cycle through the annotations present in the trajectory')
        self.cycle_annotations_action.triggered.connect(self.mainwindow.cycle_annotations)

        self.toggle_axis_action = QAction('Toggle axis', self)
        self.toggle_axis_action.setStatusTip('Show or hide the plot axis')
        self.toggle_axis_action.setCheckable(True)
        self.toggle_axis_action.setChecked(True)
        self.toggle_axis_action.triggered.connect(self.mainwindow.toggle_axis)

        self.toggle_labels_action = QAction('Toggle labels', self)
        self.toggle_labels_action.setStatusTip('Show or hide the plot labels')
        self.toggle_labels_action.setCheckable(True)
        self.toggle_labels_action.setChecked(True)
        self.toggle_labels_action.triggered.connect(self.mainwindow.toggle_labels)

        self.toggle_ticks_action = QAction('Toggle ticks', self)
        self.toggle_ticks_action.setStatusTip('Show or hide the plot ticks')
        self.toggle_ticks_action.setCheckable(True)
        self.toggle_ticks_action.setChecked(False)
        self.toggle_ticks_action.triggered.connect(self.mainwindow.toggle_ticks)

        self.export_PNG_action = QAction('as PNG', self)
        self.export_PNG_action.setStatusTip('Export the current trajectory as a PNG image')
        self.export_PNG_action.triggered.connect(self.mainwindow.export_PNG)

        self.export_CSV_action = QAction('as CSV', self)
        self.export_CSV_action.setStatusTip('Export the current trajectory as a CSV file')
        self.export_CSV_action.triggered.connect(self.mainwindow.export_CSV)

        self.evaluate_action = QAction('Evaluate', self)
        self.evaluate_action.setStatusTip('Evaluate the results of the segmentation')
        self.evaluate_action.triggered.connect(self.mainwindow.evaluate)

    def __build_menus(self):
        file_menu = self.addMenu('&File')
        file_menu.addAction(self.open_file_action)
        file_menu.addAction(self.settings_action)

        export_menu = file_menu.addMenu("Export")
        export_menu.addAction(self.export_CSV_action)
        export_menu.addAction(self.export_PNG_action)

        file_menu.addSeparator()
        file_menu.addAction(self.exit_action)

        view_menu = self.addMenu('&View')
        view_menu.addAction(self.cycle_annotations_action)
        view_menu.addAction(self.toggle_legend_action)
        view_menu.addAction(self.toggle_info_action)
        view_menu.addAction(self.toggle_axis_action)
        view_menu.addAction(self.toggle_labels_action)
        view_menu.addAction(self.toggle_ticks_action)

        compute_menu = self.addMenu('&Compute')
        compute_menu.addAction(self.segmentation_action)

        evaluate_menu = self.addMenu('&Evaluate')
        evaluate_menu.addAction(self.evaluate_action)
