import os
import time
import math
import random as rng
from datetime import datetime

import numpy as np
import glm
from PyQt5.QtCore import QObject, pyqtSignal

from ..TrajSeg.data import Trajectory
from ..data import TrajectoryData
from ..utils import QtUtils

class DataManager(QObject):

    DEFAULT_ANNOTATION = "No class"
    DEFAULT_ANNOTATION_VALUE = "unclassified"

    data_changed = pyqtSignal()

    def __init__(self, settings):
        super().__init__()
        self.settings = settings
        self.settings.settings_changed.connect(self.on_settings_change)

        self.last_path = os.path.expanduser("~")

        # clean init
        self.clear_data()

    def load_data(self, data, path, x, y, ts, ts_format, annotations, latlon):
        print("DataManager:", "load_data")
        self.clear_data()

        # file info
        self.file_path = path
        self.file_name = os.path.basename(path)

        # data format info
        self.x_label = x
        self.y_label = y
        self.ts_label = ts
        self.ts_format = ts_format
        self.annotations = []
        self.cartesian = not latlon

        self.annotation_classes = []

        # create trajectory
        try:
            self.trajectory = Trajectory.from_dict_list(data, cartesian=self.cartesian, lat=y, lon=x, ts=ts, ts_format=ts_format, annotations=annotations)
        except:
            QtUtils.display_message("Error", "Invalid data provided.")
            return

        # if there arent any annotations, add a default one
        if annotations is None:
            self.add_annotation(self.DEFAULT_ANNOTATION, [self.DEFAULT_ANNOTATION_VALUE]*len(self.trajectory), True)
        else:
        # else add the annotations one by one
            for ann in annotations:
                self.add_annotation(ann, [p.annotations[ann] for p in self.trajectory], False)

        self.is_data_loaded = True

        # build vertex data
        self.build_trajectory_data()

    # data builders
    def build_trajectory_data(self):
        print("DataManager:", "build_trajectory_data")
        colors = self.settings.cl_colors
        annotation_key = self.annotations[self.current_annotation]
        annotations_colors =  {a: colors[self.color_data[a]] for a in self.annotation_classes[self.current_annotation]}  #self.color_data #self.color_data[self.current_annotation]
        
        self.trajectory_data = TrajectoryData(self.trajectory, annotation_key, annotations_colors)   
        self.data_changed.emit()
    
    # annotations
    def add_annotation(self, key, values, add_to_traj=True):
        if add_to_traj:
            for i, p in enumerate(self.trajectory):
                p.annotations[key] = values[i]

        classes_to_add = np.unique(values)

        # if is a new annotation, add to annotations and to annotation_classes
        if key not in self.annotations:
            self.annotations.append(key)
            self.annotation_classes.append(classes_to_add)
        # else find the annotation index and add missing classes
        else:
            idx = self.annotations.index(key)
            self.annotation_classes[idx] = classes_to_add
        
        colors = self.settings.cl_colors
        color_idx = len(self.color_data)
        total_classes = np.unique(np.append(classes_to_add, list(self.color_data.keys())))

        excess = len(total_classes) - len(colors)
        if excess > 0:
            print("warning not enough colors, adding {} more".format(excess))
            for _ in range(excess):
                colors.append((rng.random(), rng.random(), rng.random(), 1.0))
        
        for cl in classes_to_add:
            if cl not in self.color_data:
                self.color_data[cl] = color_idx #colors[color_idx]
                color_idx += 1
        

        #self.build_annotations_data()
    
    def cycle_annotations(self):
        if len(self.annotations) == 1:
            return
        
        self.current_annotation = (self.current_annotation + 1 ) % len(self.annotations)
        self.build_trajectory_data()
    
    def select_annotation(self, key):
        self.current_annotation = self.annotations.index(key)
        self.build_trajectory_data()

    def get_current_annotation(self):
        if self.annotations is None:
            return None
        else:
            return self.annotations[self.current_annotation]

    def get_current_annotation_index(self):
        return self.current_annotation

    # colors
    def get_current_annotation_colors(self):
        if self.color_data is None:
            return None

        return { a: self.color_data[a] for a in self.annotation_classes[self.current_annotation] } 
    
    def get_color_data(self):
        return self.color_data

    def set_color_data(self, color_data):
        self.color_data = color_data

    # trajectory
    def get_trajectory_data(self):
        return self.trajectory_data

    def get_trajectory(self):
        return self.trajectory

    def get_x_label(self):
        return self.x_label
    
    def get_y_label(self):
        return self.y_label
    
    def get_ts_label(self):
        return self.ts_label

    def get_ts_format(self):
        return self.ts_format

    # TODO this should return None 
    def get_unique_classes(self):
        if self.annotation_classes is None:
            return ["Unclassified"]
        else:
            return self.annotation_classes[self.current_annotation]

    # slots
    def on_settings_change(self):
        print("DataManager:", "on_settings_change")
        if self.is_data_loaded:
            self.build_trajectory_data()
        else:
            self.data_changed.emit()

    # general
    def clear_data(self):
        self.file_path = None
        self.file_name = None

        self.is_data_loaded = False

        self.x_label = None
        self.y_label = None
        self.ts_label = None
        self.ts_format = None
        self.annotations = None
        self.cartesian = None

        self.current_annotation = 0

        #self.unique_classes = None
        self.annotation_classes = []
        self.color_data = {}

        self.trajectory = None
        self.trajectory_data = None
    
    def to_csv(self, path):
        if not self.is_data_loaded:
            return

        self.trajectory.export_to_csv(path, lat=self.get_y_label(), lon=self.get_x_label(), ts=self.get_ts_label(), ts_format=self.get_ts_format())
    
    # plot helpers
    def get_ray_intersection(self, ray_origin, ray_direction, radius, points_range):
        x = self.trajectory_data.get_x()
        y = self.trajectory_data.get_ts()
        z = self.trajectory_data.get_y()
        
        intersections = []
        for i in range(points_range[0], points_range[1]):
            if tc := self.__intersect_ray_sphere(ray_origin, ray_direction, radius, x[i], y[i], z[i]):
                intersections.append((i, tc))
        
        if intersections:
            idx = min(intersections, key=lambda p : p[1])[0]
            return (idx, self.trajectory[idx])
        else:
            return (-1, None)

    def __intersect_ray_sphere(self, ray_origin, ray_direction, radius, x, y, z):
        L = glm.vec3(x, y, z) - ray_origin
        tc = glm.dot(L, ray_direction)

        if tc < 0:
            return False

        d =  math.sqrt(abs(glm.dot(L, L) - tc**2))
        if d < radius:
            return tc
        else:
            return False
    
    def get_axis_scale(self):
        if self.trajectory_data is None:
            return (1, 1)

        x = max(self.trajectory_data.get_x()) - min(self.trajectory_data.get_x())
        y = max(self.trajectory_data.get_y()) - min(self.trajectory_data.get_y())
        
        if x > y:
            scale = (1, y/x)
        else:
            scale = (x/y, 1)

        return scale

    def get_x_tickmarks(self, n):
        values = [p.x for p in self.trajectory]
        return self.__get_ticks(values, n)

    def get_y_tickmarks(self, n):
        values = [p.y for p in self.trajectory]
        return self.__get_ticks(values, n)

    def get_ts_tickmarks(self, n):
        values = [p.timestamp.timestamp() for p in self.trajectory]
        res = self.__get_ticks(values, n)
        for i in range(len(res)):
            res[i] = str(datetime.fromtimestamp(float(res[i])))
        return res

    def __get_ticks(self, data, n):
        if n == 0:
            return []
            
        low = min(data)
        high = max(data)
        d = (high - low) / n

        res = []
        for i in range(n+1):
            res.append("{:0.2f}".format(low + i*d))

        return res
