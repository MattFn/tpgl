from __future__ import annotations
import json
import os
import copy

from PyQt5.QtCore import QObject, pyqtSignal

from ..data import SettingsObject

class SettingsManager(QObject):
  
    DEFAULT_BG_COLOR = (1.0, 1.0, 1.0, 1.0)
    DEFAULT_TS_FORMAT = "%Y-%m-%d %H:%M:%S"
    DEFAULT_POINTS_SIZE = 10
    DEFAULT_MOVE_LABEL = "MOVE"
    DEFAULT_STOP_LABEL = "STOP"
    DEFAULT_CLASS_COLORS = [(1.0, 0.0, 1.0, 1.0),(0.0, 1.0, 1.0, 1.0),
                            (1.0, 1.0, 0.0, 1.0),(0.0, 1.0, 0.0, 1.0),
                            (0.4, 0.2, 1.0, 1.0),(0.6, 0.2, 1.0, 1.0),
                            (1.0, 0.4, 0.2, 1.0),(0.5, 1.0, 0.5, 1.0)]

    TS_FORMAT_SETTING = "timestamp_format"
    CL_SETTING = "class" 
    BG_COLOR_SETTING = "bg_color" 
    CL_COLORS_SETTING = "class_colors"
    POINTS_SIZE_SETTING = "points_size"
    MOVE_LABEL_SETTING = "move_label"
    STOP_LABEL_SETTING = "stop_label"

    settings_changed = pyqtSignal()

    def __init__(self, settings_path: str):
        super().__init__()
        self.settings_path = settings_path
        self.load_from_file()

    def set_settings(self, settings: SettingsObject, update_view: bool = False):
        self.settings = settings
        self.save_to_file()
        self.settings_changed.emit()

    @property
    def ts_format(self):
        return self.settings.ts_format

    @property
    def cl_label(self):
        return self.settings.cl

    @property
    def bg_color(self):
        return self.settings.bg_color

    @property
    def cl_colors(self):
        return self.settings.cl_colors

    @property
    def points_size(self):
        return self.settings.points_size

    @property
    def move_label(self):
        return self.settings.move_label

    @property
    def stop_label(self):
        return self.settings.stop_label

    def copy_colors(self):
        return copy.deepcopy(self.settings.cl_colors)

    def add_cl_color(self, r, g, b):
        self.settings.cl_colors.append([r, g, b])

    def load_from_file(self):
        try:
            with open(self.settings_path, 'r') as file:
                data = json.load(file)
                self.settings = SettingsObject(
                    ts_format = data[SettingsManager.TS_FORMAT_SETTING],
                    bg_color = data[SettingsManager.BG_COLOR_SETTING],
                    cl_colors = data[SettingsManager.CL_COLORS_SETTING],
                    points_size = data[SettingsManager.POINTS_SIZE_SETTING],
                    move_label= data[SettingsManager.MOVE_LABEL_SETTING],
                    stop_label= data[SettingsManager.STOP_LABEL_SETTING]
                )

        except FileNotFoundError:
            print("WARNING: could not find or read the setting.json\n\tgenerated new one")
            self.settings = SettingsObject(ts_format=SettingsManager.DEFAULT_TS_FORMAT, bg_color=SettingsManager.DEFAULT_BG_COLOR, cl_colors=SettingsManager.DEFAULT_CLASS_COLORS, points_size=SettingsManager.DEFAULT_POINTS_SIZE, move_label=SettingsManager.DEFAULT_MOVE_LABEL, stop_label=SettingsManager.DEFAULT_STOP_LABEL)
            self.save_to_file()
        except KeyError:
            print("WARNING: invalid format for setting.json\n\tgenerated new one")
            self.settings = SettingsObject(ts_format=SettingsManager.DEFAULT_TS_FORMAT, bg_color=SettingsManager.DEFAULT_BG_COLOR, cl_colors=SettingsManager.DEFAULT_CLASS_COLORS, points_size=SettingsManager.DEFAULT_POINTS_SIZE, move_label=SettingsManager.DEFAULT_MOVE_LABEL, stop_label=SettingsManager.DEFAULT_STOP_LABEL)
            self.save_to_file()
        except json.decoder.JSONDecodeError:
            print("WARNING: invalid format for setting.json\n\tgenerated new one")
            self.settings = SettingsObject(ts_format=SettingsManager.DEFAULT_TS_FORMAT, bg_color=SettingsManager.DEFAULT_BG_COLOR, cl_colors=SettingsManager.DEFAULT_CLASS_COLORS, points_size=SettingsManager.DEFAULT_POINTS_SIZE, move_label=SettingsManager.DEFAULT_MOVE_LABEL, stop_label=SettingsManager.DEFAULT_STOP_LABEL)
            self.save_to_file()
            
    def save_to_file(self):
        print("Saving settings...")
        os.makedirs(os.path.dirname(self.settings_path), exist_ok=True)
        with open(self.settings_path, 'w') as f:
            json.dump(self.settings.serialize(), f)