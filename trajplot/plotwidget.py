import numpy as np
import time
import random as rng
import math
import ctypes
import sys
import os
from typing import List, Callable

from PyQt5.QtWidgets import QOpenGLWidget
from PyQt5.QtCore import QPoint, Qt, QTimer, pyqtSignal

import OpenGL.GL as gl
import OpenGL.GL.shaders
import glm

from .TrajSeg.data import Point
from .core import FontAtlas

class GLWidget(QOpenGLWidget):
    RESOURCES_LOCATION = "trajplot/resources"

    DEFAULT_X_LABEL = "x"
    DEFAULT_Y_LABEL = "y"
    DEFAULT_TS_LABEL = "ts"

    vertex_shader = b"""
        #version 420 core
        in layout(location = 0) vec3 aPosition;
        in layout(location = 1) vec3 aColor;
        in layout(location = 2) vec2 aTextUV;

        uniform mat4 Transform;

        out vec3 vColor;
        out vec2 vTextUV;

        void main()
        {
            gl_Position = Transform * vec4(aPosition, 1.0f);
            vColor = aColor;
            vTextUV = aTextUV;
        }
        """

    fragment_shader = b"""
        #version 420 core
        in vec3 vColor;
        in vec2 vTextUV;

        uniform int RenderMode;
        uniform sampler2D Text;

        out vec4 FragColor;

        //const float width = 0.5;
        //const float edge = 0.1;

        void main()
        {
            if (RenderMode == 0) {
                if(length(gl_PointCoord - vec2(0.5)) > 0.5)
                    discard;
                else
                    FragColor = vec4(vColor, 1.0);
            } 
            else if (RenderMode == 1) {
                FragColor = vec4(0.0, 0.0, 0.0, vColor.x);
            }
            else if (RenderMode == 2) {
                //float distance = 1.0 -  texture(Text, vTextUV).a;
                //float alpha = 1.0 - smoothstep(width, width+edge, distance);
                //FragColor = vec4(0.0, 0.0, 0.0, alpha);
                
                vec4 sampled = vec4(1.0f, 1.0f, 1.0f, texture(Text, vTextUV).a);
                FragColor = vec4(0.0f, 0.0f, 0.0f, 1.0f) * sampled;
            }
        }
        """

    ray_cast_result = pyqtSignal(int, Point)

    def __init__(self, controller, data_manager:"DataManager", settings_manager:"SettingsManager", slider):
        QOpenGLWidget.__init__(self, controller)
        self.controller = controller
        self.settings = settings_manager
        self.data_manager = data_manager

        self.setFocusPolicy(Qt.StrongFocus)
        self.mouseLastPos = QPoint()

        # rendering settings
        self.render_text = True
        self.render_axis = True
        self.render_ticks = False

        # signals binding
        self.data_manager.data_changed.connect(self.load_plot)
        slider.point_index_change.connect(self.set_index)

        # matrices
        self.projection_matrix = glm.mat4()
        self.model_matrix = glm.mat4()
        self.view_matrix = glm.mat4()

        # position, rotation and scale
        self.default_camera_position = self.camera_position = glm.vec3(0, 0, 30)
        self.model_position = glm.vec3(0, 0, 0)
        self.default_rotation = self.rotation = glm.vec3(25, 330, 0)
        self.scale = 1

        # points draw indices
        self.points_draw_start = 0
        self.points_draw_end = 0
        
        self.font_atlas = FontAtlas(os.path.join(controller.application_path, GLWidget.RESOURCES_LOCATION,"font/Verdana112.fnt"), 0.01)
    
    def initializeGL(self):
        print("GLWidget: initializeGL")
        self.shader = OpenGL.GL.shaders.compileProgram(
            OpenGL.GL.shaders.compileShader(GLWidget.vertex_shader, gl.GL_VERTEX_SHADER),
            OpenGL.GL.shaders.compileShader(GLWidget.fragment_shader, gl.GL_FRAGMENT_SHADER))
        print("\tShaders compiled")

        # set program
        gl.glUseProgram(self.shader)
        print("\tProgram set")

        # gen VAOs
        self.VAO_points = gl.glGenVertexArrays(1)
        self.VAO_axes = gl.glGenVertexArrays(1)
        self.VAO_text = gl.glGenVertexArrays(1)
        
        # gen VBOs
        self.VBO_points = gl.glGenBuffers(1)
        self.VBO_axes = gl.glGenBuffers(1)
        self.VBO_text = gl.glGenBuffers(1)

        # init VAO 1 - points
        gl.glBindVertexArray(self.VAO_points)
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.VBO_points)

        gl.glVertexAttribPointer(0, 3, gl.GL_FLOAT, gl.GL_FALSE, 24, ctypes.c_void_p(0))
        gl.glVertexAttribPointer(1, 3, gl.GL_FLOAT, gl.GL_FALSE, 24, ctypes.c_void_p(12))
        
        gl.glEnableVertexAttribArray(0)
        gl.glEnableVertexAttribArray(1)

        gl.glBindVertexArray(0)
        print("\tVAO 1 initialized")

        # init VAO 2 - axis
        gl.glBindVertexArray(self.VAO_axes)
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.VBO_axes)

        gl.glVertexAttribPointer(0, 3, gl.GL_FLOAT, gl.GL_FALSE, 16, ctypes.c_void_p(0))
        gl.glVertexAttribPointer(1, 1, gl.GL_FLOAT, gl.GL_FALSE, 16, ctypes.c_void_p(12))
        gl.glEnableVertexAttribArray(0)
        gl.glEnableVertexAttribArray(1)

        gl.glBindVertexArray(0)
        print("\tVAO 2 initialized")

        # init VAO 3 - text
        gl.glBindVertexArray(self.VAO_text)
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.VBO_text)

        gl.glVertexAttribPointer(0, 3, gl.GL_FLOAT, gl.GL_FALSE, 20, ctypes.c_void_p(0))
        gl.glVertexAttribPointer(2, 2, gl.GL_FLOAT, gl.GL_FALSE, 20, ctypes.c_void_p(12))

        gl.glEnableVertexAttribArray(0)
        gl.glEnableVertexAttribArray(2)

        self.font_atlas.load_texture(os.path.join(self.controller.application_path, GLWidget.RESOURCES_LOCATION, "font/Verdana112.png"))

        gl.glBindVertexArray(0)
        print("\tVAO 3 initialized")

        # set uniforms
        self.transform_matrix_uniform = gl.glGetUniformLocation(self.shader, "Transform")
        self.render_mode_uniform = gl.glGetUniformLocation(self.shader, "RenderMode")
        self.texture_uniform = gl.glGetUniformLocation(self.shader, "Text")
        print("\tUniforms set")

        # setting flags
        gl.glEnable(gl.GL_DEPTH_TEST)
        gl.glEnable(gl.GL_BLEND)
        gl.glEnable(gl.GL_MULTISAMPLE)
        gl.glPointSize(self.settings.points_size)
        gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)

        gl.glClearColor(*self.settings.bg_color)
        print("GLWidget: OpenGl initialized")
        self.load_plot()     
    
    def paintGL(self):
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
    
        asp = float(self.width()) / float(self.height())
        if self.width() > self.height():
            self.projection_matrix = glm.ortho((-20*asp)/self.scale, (20*asp)/self.scale, -20/self.scale, 20/self.scale, 0, 200)
        else:
            self.projection_matrix = glm.ortho(-20/self.scale, 20/self.scale, -20/self.scale, (20/asp)/self.scale, 0, 200)

        self.model_matrix = glm.rotate(glm.mat4(1.0), glm.radians(self.rotation.x), glm.vec3(1, 0, 0))
        self.model_matrix = glm.rotate(self.model_matrix, glm.radians(self.rotation.y), glm.vec3(0, 1, 0))
        self.model_matrix = glm.rotate(self.model_matrix, glm.radians(self.rotation.z), glm.vec3(0, 0, 1))
        self.model_matrix = glm.translate(self.model_matrix, glm.vec3(0, 0, 0))#"""self.model_position""")

        self.view_matrix  = glm.lookAt(
            self.camera_position,
            self.camera_position - glm.vec3(0, 0, 1),
            glm.vec3(0, 1, 0)
        )

        transform_matrix = self.projection_matrix * self.view_matrix * self.model_matrix
        gl.glUniformMatrix4fv(self.transform_matrix_uniform, 1, gl.GL_FALSE, glm.value_ptr(transform_matrix))

        # render mode 0
        gl.glBindVertexArray(self.VAO_points)
        gl.glUniform1i(self.render_mode_uniform, 0)
        gl.glDrawArrays(gl.GL_POINTS, self.points_draw_start, self.points_draw_end - self.points_draw_start)

        if self.render_axis:
            # render mode 1
            gl.glBindVertexArray(self.VAO_axes)
            gl.glUniform1i(self.render_mode_uniform, 1)
            gl.glDrawArrays(gl.GL_LINES, 0, int(self.axis_data_length/3))

        if self.render_text:
            # render mode 2
            gl.glBindVertexArray(self.VAO_text)
            gl.glUniform1i(self.render_mode_uniform, 2)
            
            gl.glBindTexture(gl.GL_TEXTURE_2D, self.font_atlas.textureID)
            gl.glUniform1i(self.texture_uniform, 0)
            gl.glDrawArrays(gl.GL_TRIANGLES, 0, 6*self.text_length)

        gl.glBindVertexArray(0)

    def build_axis(self, n, adv):
        a = 0.2
        size = 0.2
        
        axis = [
            # bottom
            -10,                            -10,    -10,                            a,      -10+20*self.axis_scale[0],  -10,    -10,                        a, 
            -10+20*self.axis_scale[0],      -10,    -10,                            1,      -10+20*self.axis_scale[0],  -10,    -10+20*self.axis_scale[1],  1, 
            -10+20*self.axis_scale[0],      -10,    -10+20*self.axis_scale[1],      1,      -10,                        -10,    -10+20*self.axis_scale[1],  1,
            -10,                            -10,    -10+20*self.axis_scale[1],      a,      -10,                        -10,    -10,                        a,
            # top
            -10,                            10,    -10,                             a,      -10+20*self.axis_scale[0],  10,    -10,                         a, 
            -10+20*self.axis_scale[0],      10,    -10,                             a,      -10+20*self.axis_scale[0],  10,    -10+20*self.axis_scale[1],   a, 
            -10+20*self.axis_scale[0],      10,    -10+20*self.axis_scale[1],       a,      -10,                        10,    -10+20*self.axis_scale[1],   a,
            -10,                            10,    -10+20*self.axis_scale[1],       a,      -10,                        10,    -10,                         a,
            #sides
            -10,                            -10,    -10,                            a,      -10,                        10,    -10,                         a,
            -10+20*self.axis_scale[0],      -10,    -10,                            1,      -10+20*self.axis_scale[0],  10,    -10,                         1,
            -10+20*self.axis_scale[0],      -10,    -10+20*self.axis_scale[1],      a,      -10+20*self.axis_scale[0],  10,    -10+20*self.axis_scale[1],   a,
            -10,                            -10,    -10+20*self.axis_scale[1],      a,      -10,                        10,    -10+20*self.axis_scale[1],   a,
        ]

        # tickmarks
        for i in range(n[0]+1):
            if i == 0 or i == n[0]:
                axis.extend( [-10+i*adv[0], -10, -10+20*self.axis_scale[1]+size, 1, -10+i*adv[0], -10, -10+20*self.axis_scale[1], 1] )
            else:
                axis.extend( [-10+i*adv[0], -10, -10+20*self.axis_scale[1]+size, 1, -10+i*adv[0], -10, -10+20*self.axis_scale[1]-size, 1] )
        
        for i in range(n[1]+1):
            if i == 0 or i == n[1]:
                axis.extend( [-10+20*self.axis_scale[0]+size, -10, -10+20*self.axis_scale[1]-i*adv[1],    1, -10+20*self.axis_scale[0], -10, -10+20*self.axis_scale[1]-i*adv[1], 1] )
            else:
                axis.extend( [-10+20*self.axis_scale[0]+size, -10, -10+20*self.axis_scale[1]-i*adv[1],    1, -10+20*self.axis_scale[0]-size, -10, -10+20*self.axis_scale[1]-i*adv[1], 1] )

        for i in range(n[2]+1):
            if i == 0 or i == n[2]:
                axis.extend( [-10+20*self.axis_scale[0]+size, -10+i*adv[2], -10,  1,  -10+20*self.axis_scale[0], -10+i*adv[2], -10, 1] )
            else:
                axis.extend( [-10+20*self.axis_scale[0]+size, -10+i*adv[2], -10,  1,  -10+20*self.axis_scale[0]-size, -10+i*adv[2], -10, 1] )
        
        return np.array(axis, dtype=np.float32)
    
    # signals handlers
    def load_plot(self):
        print("GLWidget: load_plot")

        trajectory_data = self.data_manager.get_trajectory_data()
        self.makeCurrent()

        # setting background and point size
        gl.glClearColor(*self.settings.bg_color)
        gl.glPointSize(self.settings.points_size)

        # buffering points
        if self.data_manager.is_data_loaded:
            gl.glBindVertexArray(self.VAO_points)
            gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.VBO_points)
            gl.glBufferData(gl.GL_ARRAY_BUFFER, trajectory_data.get_len_bytes(), trajectory_data.get_vertex_data(), gl.GL_DYNAMIC_DRAW)
            self.points_draw_end = trajectory_data.get_length()

        # computing model scale and position
        self.axis_scale = self.data_manager.get_axis_scale()
        self.model_position = glm.vec3(+10-10*self.axis_scale[0], 0, +10-10*self.axis_scale[1])
        
        # computing number of ticks and advance based on the axis ratio
        n = [0, 0, 5]
        adv = [0, 0, 20/n[2]]
        for i in range(2):
            if self.axis_scale[i] == 0:
                n[i] = 0
            elif self.axis_scale[i] <= 0.25:
                n[i] = 1
            elif self.axis_scale[i] <= 0.5:
                n[i] = 2
            elif self.axis_scale[i] <= 0.75:
                n[i] = 3
            elif self.axis_scale[i] <= 1:
                n[i] = 4

            adv[i] =  (20*self.axis_scale[i]) / n[i]

        #buffering axis
        axis_data = self.build_axis(n, adv)

        self.axis_data_length = len(axis_data)
        gl.glBindVertexArray(self.VAO_axes)
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.VBO_axes)
        gl.glBufferData(gl.GL_ARRAY_BUFFER, len(axis_data)*4, axis_data, gl.GL_DYNAMIC_DRAW)
        
        # buffering text
        gl.glBindVertexArray(self.VAO_text)

        x_lbl = self.data_manager.get_x_label() if self.data_manager.is_data_loaded else self.DEFAULT_X_LABEL
        y_lbl = self.data_manager.get_y_label() if self.data_manager.is_data_loaded else self.DEFAULT_Y_LABEL
        z_lbl = self.data_manager.get_ts_label() if self.data_manager.is_data_loaded else self.DEFAULT_TS_LABEL

        # axis labels 
        text_data = np.concatenate((
            self.font_atlas.get_text_vertex_data(x_lbl, glm.vec3(-9.5, -10, -10+20*self.axis_scale[1]-0.5), glm.vec3(90, 0, 0), 1, False),
            self.font_atlas.get_text_vertex_data(y_lbl, glm.vec3(-11+20*self.axis_scale[0], -10, -8.5), glm.vec3(90, 0, 0), 1, True),
            self.font_atlas.get_text_vertex_data(z_lbl, glm.vec3(-11+20*self.axis_scale[0], 8,   -10), glm.vec3(0, 0, 0), 1, True),   
            ))
        self.text_length = len(x_lbl) + len(y_lbl) + len(z_lbl)

        # timestamp ticks
        if self.data_manager.is_data_loaded and self.render_ticks:
            for i, tick in enumerate(self.data_manager.get_x_tickmarks(n[0])):
                text_data = np.concatenate((text_data, self.font_atlas.get_text_vertex_data(tick, glm.vec3(-10+i*adv[0]-0.6, -10, -10+20*self.axis_scale[1]+0.6), glm.vec3(90, 90, 0), 0.6, False)))
                self.text_length += len(tick)
            
            for i, tick in enumerate(self.data_manager.get_y_tickmarks(n[1])[::-1]):
                text_data = np.concatenate((text_data, self.font_atlas.get_text_vertex_data(tick, glm.vec3(-10+20*self.axis_scale[0]+0.5, -10, -10+i*adv[1]+0.6), glm.vec3(90, 0, 0), 0.6, False)))
                self.text_length += len(tick)
            
            for i, tick in enumerate(self.data_manager.get_ts_tickmarks(n[2])):
                text_data = np.concatenate((text_data, self.font_atlas.get_text_vertex_data(tick, glm.vec3(-10+20*self.axis_scale[0]+1.2, -10+i*adv[2], -10-0.7), glm.vec3(0, -45, -45), 0.6, False)))
                self.text_length += len(tick)   

        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.VBO_text)
        gl.glBufferData(gl.GL_ARRAY_BUFFER, len(text_data)*4, text_data, gl.GL_DYNAMIC_DRAW)
        
        self.doneCurrent()
        self.update()
     
    def set_index(self, start, end):
        self.points_draw_start = start
        self.points_draw_end = end
        self.update()

    def mousePressEvent(self, event):
        if event.modifiers() & Qt.ShiftModifier:
            self.test_cast(event.pos().x(), event.pos().y())
        self.mouseLastPos = event.pos()

    def mouseMoveEvent(self, event):
        if event.modifiers() & Qt.ShiftModifier & Qt.LeftButton:
            return

        dx = event.x() - self.mouseLastPos.x()
        dy = event.y() - self.mouseLastPos.y()

        if event.buttons() & Qt.LeftButton:
            self.setYRotation(0.1 * dx)
            self.setXRotation(0.1 * dy)
        elif event.buttons() & Qt.RightButton:
            self.camera_position += glm.vec3(-dx*0.01, dy*0.01, 0)

        self.mouseLastPos = event.pos()
        self.update()
    
    def setXRotation(self, value):
        self.rotation.x  = np.clip(self.rotation.x + value, -90, 90) 
    def setYRotation(self, value):
        self.rotation.y = (self.rotation.y + value) % 360
    def setZRotation(self, value):
        self.rotation.z = (self.rotation.z + value) % 360

    def wheelEvent(self, event):
        angle = event.angleDelta().y()
        
        if angle > 0:
            self.scale += 0.1
        if angle < 0:
            self.scale -= 0.1
 
        self.scale = np.clip(self.scale, 0.1, 10)
        self.update()

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Left:
            self.model_position += glm.vec3(-0.5, 0, 0)
        elif event.key() == Qt.Key_Right:  
            self.model_position += glm.vec3(0.5, 0, 0)
        elif event.key() == Qt.Key_Up: 
            self.model_position += glm.vec3(0, 0, 0.5)
        elif event.key() == Qt.Key_Down:
            self.model_position += glm.vec3(0, 0, -0.5)
    
        self.update()
    
    def toggle_labels(self):
        self.render_text = not self.render_text
        self.load_plot()

    def toggle_axis(self):
        self.render_axis = not self.render_axis
        self.load_plot()

    def toggle_ticks(self):
        self.render_ticks = not self.render_ticks
        self.load_plot()

    def test_cast(self, x, y):
        mouseX = (float(x) / float(self.width()) - 0.5) * 2.0
        mouseY = (float(self.height() - y) / float(self.height()) -0.5) * 2.0

        lRayStart_NDC = glm.vec4(mouseX, mouseY, -1.0, 1.0)
        lRayEnd_NDC = glm.vec4(mouseX, mouseY, 0.0, 1.0)

        InverseProjectionMatrix = glm.inverse(self.projection_matrix)
        InverseViewMatrix = glm.inverse(self.view_matrix * self.model_matrix)

        lRayStart_camera = InverseProjectionMatrix * lRayStart_NDC         

        lRayStart_world = InverseViewMatrix * lRayStart_camera     

        lRayEnd_camera = InverseProjectionMatrix * lRayEnd_NDC       

        lRayEnd_world = InverseViewMatrix * lRayEnd_camera       

        lRayDir_world = glm.vec3(lRayEnd_world - lRayStart_world)
        lRayDir_world = glm.normalize(lRayDir_world)

        out_origin = glm.vec3(lRayStart_world)
        out_direction = glm.normalize(lRayDir_world)

        asp = float(self.width()) / float(self.height())
        if asp > 1:
            dist = ((20*asp)/self.scale) * 2
        else:
            dist = ((20)/self.scale) * 2
        
        p_size = self.settings.points_size
        w = self.width()

        radius = ((p_size * dist) / w) / 2
        index, point = self.data_manager.get_ray_intersection(out_origin, out_direction, radius, (self.points_draw_start, self.points_draw_end))

        if point is not None:
            self.ray_cast_result.emit(index, point)

            
