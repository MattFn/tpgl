import shlex
import numpy as np
import glm
import OpenGL.GL as gl

from PyQt5.QtGui import QImage


class Character:

    def __init__(self, id, width, height, x_offset, y_offset, x_advance, uv_x, uv_y, uv_w, uv_h):
        self.id = id       
        self.width = width   
        self.height = height   
        self.x_offset = x_offset    
        self.y_offset = y_offset   
        self.x_advance = x_advance
        self.uv_x = uv_x 
        self.uv_y = uv_y 
        self.uv_w = uv_w 
        self.uv_h = uv_h

class FontAtlas:

    PAD_TOP = 0
    PAD_LEFT = 1
    PAD_BOTTOM = 2
    PAD_RIGHT = 3
    
    def __init__(self, path, scale):
        self.characters = {}
        self.kernings = {}

        self.font_scale = scale

        with open(path) as f:
            self.font_file = f.readlines()
        
        self.parse_font_file()

    def load_texture(self, texture_file):       
        img = QImage(texture_file)
        size = img.sizeInBytes()
        
        texture_data = np.array(img.constBits().asarray(size))
        
        gl.glActiveTexture(gl.GL_TEXTURE0)
        self.textureID = gl.glGenTextures(1)
        
        gl.glBindTexture(gl.GL_TEXTURE_2D, self.textureID)
        gl.glPixelStorei(gl.GL_UNPACK_ALIGNMENT, 1)

        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_S, gl.GL_CLAMP_TO_EDGE)
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_T, gl.GL_CLAMP_TO_EDGE)
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MIN_FILTER, gl.GL_LINEAR_MIPMAP_LINEAR)
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_LINEAR)
        
        gl.glTexImage2D(gl.GL_TEXTURE_2D, 0, gl.GL_RGBA, self.common["scaleW"], self.common["scaleH"], 0, gl.GL_RGBA, gl.GL_UNSIGNED_BYTE, texture_data)

        gl.glGenerateMipmap(gl.GL_TEXTURE_2D)
        aniso = gl.glGetFloatv(gl.GL_MAX_TEXTURE_MAX_ANISOTROPY)
        gl.glTexParameterf(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAX_ANISOTROPY, aniso)
        
    def get_text_vertex_data(self, text, position, rotation, scale, align_right):
        
        vertex_data = []
        uv_data = []

        # compute vertex data
        cursor = position.x
        for char in text:
            c = self.characters[char]

            px = cursor + c.x_offset*scale
            py = self.line_height + position.y - c.y_offset*scale

            vertex_data.extend([
                (px,                py,            position.z),   
                (px+c.width*scale,  py,            position.z),    
                (px+c.width*scale,  py-c.height*scale,   position.z),
                 
                (px+c.width*scale,  py-c.height*scale,   position.z),
                (px,                py-c.height*scale,   position.z),
                (px,                py,            position.z),
            ])

            uv_data.extend([
                (c.uv_x,             c.uv_y),   
                (c.uv_x+c.uv_w,      c.uv_y),     
                (c.uv_x+c.uv_w,      c.uv_y+c.uv_h), 
                 
                (c.uv_x+c.uv_w,      c.uv_y+c.uv_h), 
                (c.uv_x,             c.uv_y+c.uv_h),  
                (c.uv_x,             c.uv_y),  
            ])

            cursor += (c.x_advance * scale)

        if align_right:
            translation = glm.vec3(vertex_data[-3][0], 0, 0) - glm.vec3(vertex_data[1][0], 0, 0)
            for i in range(len(vertex_data)):
                vertex_data[i] -= translation

        # rotate
        vertex_data = self.__transform_geom(vertex_data, position, scale, rotation)

        res = np.array([[*v, *u] for v, u in zip(vertex_data, uv_data)], dtype=np.float32)
        return res.flatten()
    
    def __transform_geom(self, vertices, origin, scale, angles):
        res = []

        origin = glm.vec4(origin, 0)

        mat = glm.rotate(glm.mat4(1), glm.radians(angles.z), glm.vec3(0, 0, 1))
        mat = glm.rotate(mat, glm.radians(angles.x), glm.vec3(1, 0, 0))
        mat = glm.rotate(mat, glm.radians(angles.y), glm.vec3(0, 1, 0))

        for v in vertices:
            v = glm.vec4(v, 0) - origin
            v = v * mat
            v += origin

            res.append(list(v.xyz))

        return np.array(res, dtype=np.float32)


    # meta file parsing functions
    def parse_font_file(self):
        for line in self.font_file:
            row = shlex.split(line)
             
            if row[0] == "info":
                self.info = self.__get_dict(row[1:])
                self.info["padding"] = [int(p) for p in self.info["padding"].split(",")]
                self.info["spacing"] = [int(p) for p in self.info["spacing"].split(",")]
                self.w_padding = self.info["padding"][self.PAD_LEFT] + self.info["padding"][self.PAD_RIGHT]
                self.h_padding = self.info["padding"][self.PAD_TOP] + self.info["padding"][self.PAD_BOTTOM]
            elif row[0] == "common":
                self.common = self.__get_dict(row[1:])
                self.line_height = (self.common["lineHeight"] - self.h_padding)* self.font_scale
            elif row[0] == "page":
                self.page = self.__get_dict(row[1:])
            elif row[0] == "chars":
                self.chars = self.__get_dict(row[1:])
            elif row[0] == "char":
                self.build_char(row[1:])
            elif row[0] == "kernings":
                self.kernings = self.__get_dict(row[1:])
            elif row[0] == "kerning":
                self.build_kern(row[1:])
            else:
                raise KeyError("Unexpected value '{}' found".format(row[0]))

    def build_char(self, row):
        scale_w = self.common["scaleW"]
        scale_h = self.common["scaleH"]
        values = self.__get_dict(row)
        c = Character(
            id          = values["id"],
            uv_x        = (values["x"] + self.info["padding"][self.PAD_LEFT]) / scale_w,
            uv_y        = (values["y"] + self.info["padding"][self.PAD_TOP]) / scale_h,
            uv_w        = (values["width"] - self.w_padding) / scale_w,
            uv_h        = (values["height"] - self.h_padding) / scale_h,
            height      = (values["height"] - self.w_padding) * self.font_scale,
            width       = (values["width"] - self.h_padding) * self.font_scale,
            x_offset    = (values["xoffset"] - self.info["padding"][self.PAD_LEFT]) * self.font_scale,
            y_offset    = (values["yoffset"] - self.info["padding"][self.PAD_TOP]) * self.font_scale,
            x_advance   = (values["xadvance"] - self.w_padding) * self.font_scale,
            )

        self.characters[chr(c.id)] = c

    def build_kern(self, row):
        values = self.__get_dict(row)
        k = "{},{}".format(values["first"], values["second"])

        self.kernings[k] = values["amount"]*self.font_scale

    def __get_dict(self, row):
        res = {}
        for item in row:
            k, v = item.split("=")
            try:
                res[k] = int(v)
            except:
                res[k] = v

        return res