from PyQt5.QtCore import pyqtSignal, QThread

class AlgorithmWorker(QThread):
    
    update_progress_signal = pyqtSignal(int)
    computation_done_signal = pyqtSignal()

    def __init__(self, algorithm, params):
        super().__init__()

        self.algorithm = algorithm
        self.params = params

        self.progress = 0
        self.algorithm.update_progress = self.update_progress

    def run(self):
        self.output = self.algorithm.run(**self.params)
        self.computation_done_signal.emit()

    def get_output(self):
        return [p.annotations["class"] for p in self.output]
    
    def update_progress(self, completion):
        if self.progress == 0 or self.progress != int(completion):
            self.progress = int(completion)
            self.update_progress_signal.emit(self.progress)

    def get_algorithm_name(self):
        return type(self.algorithm).__name__