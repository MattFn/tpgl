import sys
import os
from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QSurfaceFormat, QIcon
from PyQt5.QtWinExtras import QtWin

from trajplot.mainwindow import TrajPlotGL

def run_application():
    frmat = QSurfaceFormat() 
    frmat.setDepthBufferSize(24)
    frmat.setVersion(4, 2)
    frmat.setSamples(8)
    frmat.setProfile(QSurfaceFormat.CoreProfile)
    QSurfaceFormat.setDefaultFormat(frmat)

    app = QApplication(sys.argv)

    # setting icon
    myappid = 'mycompany.myproduct.subproduct.version'
    QtWin.setCurrentProcessExplicitAppUserModelID(myappid)

    icon_path = sys._MEIPASS if getattr(sys, 'frozen', False) else sys.path[0]
    app.setWindowIcon(QIcon(os.path.join(icon_path, 'trajplot/resources/trajploticon.ico')))
    
    main_window = TrajPlotGL()
    main_window.show()
    sys.exit(app.exec_())
