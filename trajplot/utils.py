import os
import re
from datetime import datetime

from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtCore import QRect


class QtUtils:

    @staticmethod
    def display_message(title, message, mode=0):
        if mode == 1:
            QMessageBox.warning(None, title, message)
        elif mode == 2:
            QMessageBox.information(None, title, message)
        else:
            QMessageBox.critical(None, title, message)

    @staticmethod
    def display_question(title, message):
        return QMessageBox.question(None, title, message, QMessageBox.Ok|QMessageBox.Cancel)

    @staticmethod
    def create_output_filename(export_folder, input_file_name, file_format):
        name = input_file_name.split(".")[0:-1]
        name = ".".join(name) + file_format
        output_file = os.path.join(export_folder, name)
        i = 1
        while os.path.isfile(output_file):
            name = input_file_name.split(".")[0:-1]
            name = ".".join(name) + "_" + str(i) + file_format
            output_file = os.path.join(export_folder, name)
            i += 1
        
        return output_file

    @staticmethod
    def create_dialog_geom(parent_geom, width, height):
        rect = QRect(0, 0, width, height)
        rect.moveCenter(parent_geom.center())
        
        return rect

class Utils:

    @staticmethod
    def guess_date_format(input_date):
        date = None
        date_sep = None
        date_time_sep = ""
        time_seps = []

        date_format = ""
        index = 0
        # find date
        for c in input_date:
            if c.isdigit():
                pass
            else:
                if re.fullmatch(r"\d+{0}\d+{0}\d+.*".format(c), input_date):
                    date = re.match(r"\d+{0}\d+{0}\d+".format(c), input_date).group(0)
                    date_sep = c
                    break

        if date is None:
            return None

        index = len(date)

        # date type
        splt = date.split(date_sep)
        if len(splt[0]) == 4: # year first
            date_format += "%Y{0}%m{0}%d".format(date_sep) 
        else: # or day first, dont care for other options
            date_format += "%d{0}%m{0}%Y".format(date_sep) 
            
        # find date - time separator
        for c in input_date[index:]:
            if not c.isdigit():
                date_time_sep += c
                index += 1
            else:
                break

        date_format += date_time_sep

        # find time separators
        for c in input_date[index:]:
            if c.isdigit():
                pass
            else:
                time_seps.append(c)

        # time format
        if time_seps:
            for i in range(len(time_seps)+1):
                if i == 0:
                    date_format += "%H"
                if i == 1:
                    date_format += "{}%M".format(time_seps[i-1])
                if i == 2:
                    date_format += "{}%S".format(time_seps[i-1])
                if i == 3:
                    date_format += "{}%f".format(time_seps[i-1])

        # try to parse
        try:
            datetime.strptime(input_date, date_format)
            return date_format
        except:
            return None

class Point:

    def __init__(self, id, x, y, ts, cl1, cl2):
        self.id = id
        self.x = x
        self.y = y
        self.ts = ts
        self.cl1 = cl1
        self.cl2 = cl2