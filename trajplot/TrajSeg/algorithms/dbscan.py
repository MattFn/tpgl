import numpy as np

from ..data import Trajectory

class DBSCAN:

    STOP_LABEL = "STOP"
    MOVE_LABEL = "MOVE"

    def __init__(self, trajectory:Trajectory):      
        self.trajectory = trajectory     

    def run(self, eps, min_points):
        print("Launching DBSCAN with\n\teps: {}\n\tmin_points: {}".format(eps, min_points))
        self.classification = ['UNCLASSIFIED'] * len(self.trajectory)
        self.cluster_index = 0
        for i in range(len(self.trajectory)):
            if self.classification[i] == 'UNCLASSIFIED':
                seed = self.get_neighbours(i, eps)
                if not self.is_core_point(seed, min_points):
                    self.classification[i] = self.MOVE_LABEL 
                else:
                    self.cluster_index  += 1
                    for j in seed + [i]: 
                        self.classification[j] = "{}_{}".format(self.STOP_LABEL, self.cluster_index)
                    self.expand_cluster(i, seed, eps, min_points)

        return self.prepare_output()

    def is_core_point(self, neighbours, min_points):
        return len(neighbours) >= min_points

    def expand_cluster(self, p_index, seed, eps, min_points):
        for i in seed:
            neighbours = self.get_neighbours(i, eps)
            if self.is_core_point(neighbours, min_points):
                for j in neighbours:
                    if self.classification[j] in ('UNCLASSIFIED', self.MOVE_LABEL):
                        if self.classification[j] == 'UNCLASSIFIED':
                            seed.append(j)
                        self.classification[j] = "{}_{}".format(self.STOP_LABEL, self.cluster_index)
    
    def get_neighbours(self, p_index, eps):
        res = []
        for i in range(len(self.trajectory)):      
            if self.trajectory.distance(p_index, i) < eps:
                res.append(i)
        
        return res 

    def prepare_output(self):
        annotated_traj = Trajectory(cartesian=self.trajectory.is_cartesian)
        for i, p in enumerate(self.trajectory):
            annotated_traj.add_point(p)
            annotated_traj[i].annotations["class"] = self.classification[i]

        return annotated_traj
        