from __future__ import annotations
from ..data import Trajectory

class SWSPD():

    STOP_LABEL = "STOP"
    MOVE_LABEL = "MOVE"

    def __init__(self, trajectory:Trajectory, silent=True):
        self.trajectory = trajectory
        self.silent = silent
        self.merge_threshold = 0.5
        self.current_cluster_exit = None
        self.current_cluster_entry = None
        self.current_cluster = 1
        self.classification = None

    def run(self, distance_threshold, time_threshold):
        anchor = 0
        current = 0
        self.classification = [-1] * len(self.trajectory)
        while current < len(self.trajectory):
            if not self.silent: 
                self.update_progress((current*100)/len(self.trajectory))
            # or condizione ultimo punto
            if (self.current_cluster_exit and anchor > self.current_cluster_exit) or (current == len(self.trajectory)-1 and self.current_cluster_entry):
                
                self.current_cluster_exit = None
                self.current_cluster_entry = None
                self.current_cluster += 1

            d = self.trajectory.distance(anchor, current)
            
            if d > distance_threshold:
                t = self.trajectory.delta_time(anchor, current-1).total_seconds()
                if t > time_threshold:
                    self.addCluster(anchor, current-1)
                
                anchor += 1
            else:
                current = current+1

        if not self.silent: 
            self.update_progress(100)

        return self.prepare_output()

    def prepare_output(self):
        annotated_traj = Trajectory(cartesian=self.trajectory.is_cartesian)
        for i, p in enumerate(self.trajectory):
            annotated_traj.add_point(p)
            if self.classification[i] != -1:
                annotated_traj[i].annotations["class"] = "{}_{}".format(self.STOP_LABEL, self.classification[i])
            else:
                annotated_traj[i].annotations["class"] = self.MOVE_LABEL

        return annotated_traj

    def addCluster(self, first, last):
        if self.classification[first] != -1:
            self.current_cluster = self.classification[first]

        for i in range(first, last+1):
            self.classification[i] = self.current_cluster
        
        if not self.current_cluster_exit:
            self.current_cluster_entry = first

        self.current_cluster_exit = last

    def update_progress(self, completion):
        pass

class Zone:
    def __init__(self, geometry, cluster_index, timestamp):
        self.geometry = geometry
        self.clusters = [cluster_index]
        self.timestamps = [timestamp]

    def getSimilarity(self, zone: Zone):
        intersection = self.geometry.intersection(zone.geometry).area
        area = min(self.geometry.area, zone.geometry.area)

        return intersection / area

    def merge(self, zone: Zone):
        self.geometry = self.geometry.union(zone.geometry)
        self.clusters += zone.clusters
        self.timestamps += zone.timestamps


