import numpy as np

from ..data import Trajectory
from sklearn.cluster import DBSCAN

class DBSCAN_fast:

    STOP_LABEL = "STOP"
    MOVE_LABEL = "MOVE"

    def __init__(self, trajectory:Trajectory):      
        self.trajectory = trajectory     

    def run(self, eps, min_points):
        print("Launching DBSCAN with\n\teps: {}\n\tmin_points: {}".format(eps, min_points))
        self.update_progress(0)
        points  = []
        for p in self.trajectory:
            points.append([p.x, p.y])

        self.classification = DBSCAN(eps=eps, min_samples=min_points, n_jobs=2).fit(points).labels_
        self.update_progress(100)
        return self.prepare_output()

    def prepare_output(self):
        annotated_traj = Trajectory(cartesian=self.trajectory.is_cartesian)
        for i, p in enumerate(self.trajectory):
            annotated_traj.add_point(p)
            if self.classification[i] == -1:
                annotated_traj[i].annotations["class"] = self.MOVE_LABEL
            else:
                annotated_traj[i].annotations["class"] = "{}_{}".format(self.STOP_LABEL, self.classification[i])

        return annotated_traj

    def update_progress(self, completion):
        pass
        