from ..data import Trajectory

class SPD():

    UNPROCESSED = "unprocessed"
    PROCESSED = "processed"

    STOP_LABEL = "STOP"
    MOVE_LABEL = "MOVE"

    def __init__(self, trajectory:Trajectory, silent=True):      
        self.trajectory = trajectory
        self.silent = silent

    def run(self, d_thres, t_thres):
        print("Launching SPD with\n\td_thres: {}\n\tt_thres: {}".format(d_thres, t_thres))
        self.classification = [self.MOVE_LABEL] * len(self.trajectory)
        self.staypoints = []

        i = j = 0
        while i < len(self.trajectory): 
            if not self.silent: 
                self.update_progress((i*100)/len(self.trajectory))

            token = 0  
            j = i + 1

            while j < len(self.trajectory):            
                dist = self.trajectory.distance(i, j)
                dTime = self.trajectory.delta_time(i, j).total_seconds()
                
                if dist > d_thres:
                    if dTime > t_thres:
                        self.add_staypoint(i, j)
                        i = j
                        token = 1
                    break

                elif j == len(self.trajectory)-1 and dTime > t_thres:        
                    self.add_staypoint(i, j)
                    i = j
                    token = 1
                    break
                j += 1

            if token != 1:
                i += 1
        
        if not self.silent: 
            self.update_progress(100)
        
        return self.prepare_output()

    def computeMeanCoord(self, start, end):
        x = y = c = 0
        for i in range(start, end):
            x += self.trajectory[i].x
            y += self.trajectory[i].y
            c += 1

        return (x/c, y/c)

    def add_staypoint(self, start, end):
        sp_id = len(self.staypoints) + 1
        point = self.computeMeanCoord(start, end)

        arrival_time = self.trajectory[start].timestamp
        leave_time = self.trajectory[end].timestamp

        time = leave_time - arrival_time
        self.staypoints.append({
            "id": sp_id, 
            "x": point[0], 
            "y": point[1], 
            "arrival_time": arrival_time, 
            "leave_time": leave_time,
            "time": time
        })

        for i in range(start, end):
            self.classification[i] = "{}_{}".format(self.STOP_LABEL, sp_id)

    def prepare_output(self):
        annotated_traj = Trajectory(cartesian=self.trajectory.is_cartesian)
        for i, p in enumerate(self.trajectory):
            annotated_traj.add_point(p)
            annotated_traj[i].annotations["class"] = self.classification[i]

        return annotated_traj

    def update_progress(self, completion):
        pass

