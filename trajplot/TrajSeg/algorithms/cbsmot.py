import numpy as np
import math
from datetime import datetime

from ..data import Trajectory

class CBSMoT:

    UNPROCESSED = "unprocessed"
    PROCESSED = "processed"

    MOVE_LABEL = "MOVE"
    STOP_LABEL = "STOP"

    def __init__(self, trajectory:Trajectory, silent=True):
        self.trajectory = trajectory
        self.silent = silent

    def run(self, min_time, eps):
        print("Launching CB-SMoT with\n\teps: {}\n\tmin_time: {}".format(eps, min_time))

        self.classification = [self.UNPROCESSED] * len(self.trajectory)

        self.cluster_index = 1
        for p in range(len(self.trajectory)):
            if not self.silent: 
                self.update_progress((p*100)/len(self.trajectory))
            # for each unprocessed points
            if self.classification[p] == self.UNPROCESSED:
                neighbours = self.get_linear_neighbours(p, eps)
                # if p is corepoint wrt eps, min_time
                if self.is_core_point(neighbours, min_time):
                    # for each neighbour
                    for n in neighbours:
                        # add to neighbours every unprocessed point in lin_neigh of n
                        ext_neighbours = self.get_linear_neighbours(n, eps)
                        for i in ext_neighbours:
                            if self.classification[i] == self.UNPROCESSED:
                                self.classification[i] = self.PROCESSED
                                neighbours.append(i)
                        self.classification[n] = self.cluster_index
                    self.cluster_index += 1
        if not self.silent: 
            self.update_progress(100)

        return self.prepare_output()
    
    def is_core_point(self, neighbours, min_time):
        time = self.trajectory.delta_time(neighbours[0], neighbours[-1]).total_seconds()
        if  time >= min_time:
            return True
        
        return False

    def get_linear_neighbours(self, p_index, eps):
        res = []

        dist = 0
        if p_index > 0:
            for i in range(p_index-1, -1, -1):
                d = self.trajectory.distance(i, i+1)           
                if d + dist > eps:
                    break
                res.insert(0, i)
                dist += d
        
        res.append(p_index)

        dist = 0
        if p_index < len(self.trajectory) - 1:
            for i in range(p_index + 1, len(self.trajectory)):
                d = self.trajectory.distance(i, i-1)
                if d + dist > eps:
                    break
                res.append(i)
                dist += d
        
        return res 

    def computeEPS(self, area):
        dist = []
        for i in range(len(self.trajectory)-1):
            dist.append(self.trajectory.distance(i, i+1))
        
        #quantile
        #eps = np.quantile(dist, area)
        #return eps

        return area

    def prepare_output(self):
        annotated_traj = Trajectory(cartesian=self.trajectory.is_cartesian)
        for i, p in enumerate(self.trajectory):
            annotated_traj.add_point(p)

            if self.classification[i] == self.UNPROCESSED or self.classification[i] == self.PROCESSED:
                annotated_traj[i].annotations["class"] = self.MOVE_LABEL
            else:
                annotated_traj[i].annotations["class"] = "{}_{}".format(self.STOP_LABEL, self.classification[i])

        return annotated_traj

    def update_progress(self, completion):
        pass