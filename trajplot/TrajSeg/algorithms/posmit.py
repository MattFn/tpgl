import numpy as np
from sklearn.cluster import KMeans

from ..data import Trajectory
from ..utils import math_utils

class POSMIT():

    STOP_LABEL = "STOP"
    MOVE_LABEL = "MOVE"

    def __init__(self, trajectory:Trajectory, silent=True):      
        self.trajectory = trajectory
        self.silent = silent

    def run(self, nSearchRadius=None, stopVariance=None, minStopConfidence=None):
        print("Launching POSMIT with\n\tnSearchRadius: {}\n\tstopVariance: {}\n\tminStopConfidence: {}".format(
            nSearchRadius if nSearchRadius is not None else "estimate",
            stopVariance if stopVariance is not None else "estimate",
            minStopConfidence if minStopConfidence is not None else "estimate"
            ))

        if stopVariance is None:
            stopVariance = self.estimate_stop_variance()
            print("estimated stopVariance as", stopVariance)

        if nSearchRadius is None:
            nSearchRadius = self.estimate_search_radius(stopVariance)
            print("estimated nSearchRadius as", nSearchRadius)

        stopProbabilities = self.get_stop_probabilities(nSearchRadius, stopVariance)

        if minStopConfidence is None:
            minStopConfidence = self.estimate_min_stop_pr(stopProbabilities)
            print("estimated minStopConfidence as", minStopConfidence)
        
        if not self.silent: 
            self.update_progress(100)

        return self.classify_points(stopProbabilities, minStopConfidence)

    def get_stop_probabilities(self, nSearchRadius, stopVariance):
        stopProbabilities = [None] * len(self.trajectory) 
        
        for centerIdx in range(len(self.trajectory)):
            if not self.silent: 
                self.update_progress((centerIdx*100)/len(self.trajectory))

            stopProbabilities[centerIdx] = self._get_stop_probability(centerIdx, nSearchRadius, stopVariance)

        return stopProbabilities
    
    def _get_stop_probability(self, center_idx, search_radius, stop_variance):
        sum_weights = 0
        sum_index_weight = 0
        last_idx = len(self.trajectory) - 1

        bounds = [center_idx, center_idx]
        increments = [-1, 1]

        cutoff = math_utils.gaussian(3.0, 1.0, 0.0, 1.0)
        while increments[0] != 0 or  increments[1] != 0:
            for i in range(2):
                increment = increments[i]
                if increment == 0:
                    continue

                idx = bounds[i] + increment
                if idx >= 0 and idx <= last_idx:
                    index_score = abs(idx - center_idx) / float(search_radius)
                    index_weight = self.kernel(index_score)
                    if index_weight < cutoff:
                        increments[i] = 0
                    else:
                        score = self.get_score(center_idx, idx, stop_variance)
                        sum_weights += index_weight * score
                        sum_index_weight += index_weight
                        bounds[i] = idx
                else:
                    increments[i] = 0
                
        return sum_weights / sum_index_weight
    
    def classify_points(self, stop_probabilities, minStopConfidence):
        annotated_traj = Trajectory(cartesian=self.trajectory.is_cartesian)

        stop_counter = 0
        last_class = None
        for i, p in enumerate(self.trajectory):
            annotated_traj.add_point(p)
            annotated_traj[i].annotations["stop_pr"] = stop_probabilities[i]
            if stop_probabilities[i] >= minStopConfidence:
                if last_class is None or last_class == "MOVE":
                    stop_counter += 1
                annotated_traj[i].annotations["class"] = "{}_{}".format(self.STOP_LABEL, stop_counter)
                last_class = "{}_{}".format(self.STOP_LABEL, stop_counter)
            else:
                annotated_traj[i].annotations["class"] = self.MOVE_LABEL
                last_class = self.MOVE_LABEL

        return annotated_traj

    def get_score(self, idx1, idx2, stop_variance):
        if stop_variance == 0:
            return 0

        displacement = self.trajectory.distance(idx1, idx2)                              
        x = displacement / stop_variance
        return self.kernel(x)

    def kernel(self, x):
        return math_utils.gaussian(x, 1.0, 0.0, 1.0)

    # heuristics
    def estimate_search_radius(self, stop_variance):
        chunk_sizes = []

        i = 0
        while i < len(self.trajectory):
            prev_idx = i
            cur_chunk_size = 1
            i += 1

            while i < len(self.trajectory):
                displacement = self.trajectory.distance(prev_idx, i) 
                if displacement > stop_variance:
                    break

                cur_chunk_size += 1
                prev_idx = i
                i += 1

            if cur_chunk_size > 1:
                chunk_sizes.append(cur_chunk_size)
            i += 1

        if not chunk_sizes:
            return 1
        else:
            search_radius = sum(chunk_sizes) / len(chunk_sizes)
            search_radius = max(1, round(search_radius * 0.5))
            return int(search_radius)

    def estimate_min_stop_pr(self, stop_probabilities):
        kMeans = KMeans(n_clusters=2).fit_predict(np.array(stop_probabilities).reshape(-1, 1))
        cl1 = []
        cl2 = []
        for i, l in enumerate(kMeans):
            if l == 0:
                cl1.append(stop_probabilities[i])
            else:
                cl2.append(stop_probabilities[i])

        if not cl1 or not cl2:
            return 0

        cl1Max = max(cl1)
        cl2Min = min(cl2)
        
        return cl1Max + (cl2Min - cl1Max) * 0.5

    def estimate_stop_variance(self, max_stop_variance=20):
        res = [0]
        for i in range(1, len(self.trajectory)-1):
            v = self.trajectory.distance(i-1, i)
            if v > 0 and v < max_stop_variance: 
                res.append(v)

        res.sort()
        return math_utils.findElbowQuick(res) if len(res) > 1 else 0

    def update_progress(self, completion):
        pass