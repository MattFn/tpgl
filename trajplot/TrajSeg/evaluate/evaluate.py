from sklearn.metrics import adjusted_rand_score
import math

def compute_ari(labels_truth, labels_predicted):
        ari = adjusted_rand_score(labels_truth, labels_predicted)
        return ari

def compute_MCC(labels_truth, labels_predicted, false_label="MOVE"):
    tp = tn = fn = fp = 0
    for i in range(len(labels_truth)):
        if labels_truth[i] != false_label and labels_predicted[i] != false_label:
            tp += 1
        elif labels_truth[i] == false_label and labels_predicted[i] == false_label:
            tn += 1
        elif labels_truth[i] != false_label and labels_predicted[i] == false_label:
            fn += 1
        elif labels_truth[i] == false_label and labels_predicted[i] != false_label:
            fp += 1
        else:
            raise ValueError("Non dovrebbe succedere")

    try:
        mcc = ( (tp * tn) - (fp * fn) ) / math.sqrt( (tp + fp) * (tp + fn) * (tn + fp) * (tn + fn) )
        return mcc
    except ZeroDivisionError:
        return 0

def compute_FScore(labels_truth, labels_predicted, false_label="MOVE"):
    tp = tn = fn = fp = 0
    for i in range(len(labels_truth)):
        if labels_truth[i] != false_label and labels_predicted[i] != false_label:
            tp += 1
        elif labels_truth[i] == false_label and labels_predicted[i] == false_label:
            tn += 1
        elif labels_truth[i] != false_label and labels_predicted[i] == false_label:
            fn += 1
        elif labels_truth[i] == false_label and labels_predicted[i] != false_label:
            fp += 1
        else:
            raise ValueError("Non dovrebbe succedere")

    try:
        p = tp / (tp+fp)
        r = tp / (tp+fn)
    except ZeroDivisionError:
        return 0

    return 2 * (p*r) / (p+r)

def compute_stop_coverage_best_match(gt_stops, res_stops):
    gt_entries = __get_stop_entry(gt_stops)
    res_entries = __get_stop_entry(res_stops)

    stops_overlaps = __get_stops_overlaps(gt_entries, res_entries)

    res = []
    for stop_overlap in stops_overlaps:
        overlaps = stop_overlap["overlaps"]
        if overlaps:
            best_match = max(overlaps, key= lambda x : x["score"])
            if best_match['score'] > 0:
                res.append({
                    "Stop": stop_overlap['gt_stop']["id"],
                    "StopDuration": stop_overlap['gt_stop']['duration'],
                    "StopArrival": stop_overlap['gt_stop']['in'],
                    "StopLeave": stop_overlap['gt_stop']['out'],
                    "MatchingResultStop": best_match["res_stop"]["id"],
                    "MatchingResultStopDuration": best_match["res_stop"]["duration"],
                    "MatchingResultStopArrival": best_match["res_stop"]["in"],
                    "MatchingResultStopLeave": best_match["res_stop"]["out"],
                    "OverlapScore": best_match["score"],
                    "OverlapTime": best_match["duration"],
                })
            else:
                res.append({
                    "Stop": stop_overlap['gt_stop']["id"],
                    "StopDuration": stop_overlap['gt_stop']['duration'],
                    "StopArrival": stop_overlap['gt_stop']['in'],
                    "StopLeave": stop_overlap['gt_stop']['out'],
                    "MatchingResultStop": "None",
                    "MatchingResultStopDuration": "None",
                    "MatchingResultStopArrival": "None",
                    "MatchingResultStopLeave": "None",
                    "OverlapScore": 0,
                    "OverlapTime": 0,
                })
        else:
            return 0

    return res

def compute_stop_coverage_average_match(gt_stops, res_stops):
    gt_entries = __get_stop_entry(gt_stops)
    res_entries = __get_stop_entry(res_stops)

    stops_overlaps = __get_stops_overlaps(gt_entries, res_entries)

    res = []
    for stop_overlap in stops_overlaps:
        overlaps = stop_overlap["overlaps"]
        if overlaps:
            overlapping_stops_names = []
            overlapping_stops_scores = []
            for o in overlaps:
                if o['score'] > 0:
                    overlapping_stops_names.append(o['res_stop']['id'])
                    overlapping_stops_scores.append(o['score'])
            if overlapping_stops_scores:
                total_score = sum(overlapping_stops_scores) / len(overlapping_stops_scores)
                res.append({
                    "Stop": stop_overlap['gt_stop']["id"],
                    "OverlappingStops": ", ".join(overlapping_stops_names),
                    "OverlappingStopsScores": ", ".join(str(x) for x in overlapping_stops_scores),
                    "OverlapScore": total_score
                })
            else:
                res.append({
                    "Stop": stop_overlap['gt_stop'],
                    "OverlappingStops": "None",
                    "OverlappingStopsScores": "None",
                    "OverlapScore": 0
                })

        else:
            return 0

    return res

def overlap_jaccard_score(gt_start, gt_end, res_start, res_end):
    intersection_start = max(gt_start, res_start)
    intersection_end = min(gt_end, res_end)

    union_start = min(gt_start, res_start)
    union_end = max(gt_end, res_end)

    intersection = (intersection_end - intersection_start).total_seconds()
    union = (union_end - union_start).total_seconds()

    return intersection / union   

def overlap_time(gt_start, gt_end, res_start, res_end):
    overlap_start = max(gt_start, res_start)
    overlap_end = min(gt_end, res_end)

    return (overlap_end - overlap_start).total_seconds()

def __get_stop_entry(stops):
    entries = []
    for name, stop in stops.items():
        t1 = stop[0].timestamp
        t2 = stop[-1].timestamp
        duration = (t2-t1).total_seconds()

        entry = {
            "id": name,
            "duration": duration,
            "in": t1,
            "out": t2,
        }

        entries.append(entry)
    return entries

def __get_stops_overlaps(gt_entries, res_entries):
    stops_overlaps = []  
    for gt_entry in gt_entries:
        overlaps = {"gt_stop": gt_entry, "overlaps": []}
        for res_entry in res_entries:
            gt_start, gt_end = (gt_entry["in"], gt_entry["out"])
            res_start, res_end = (res_entry["in"], res_entry["out"])

            score = overlap_jaccard_score(gt_start, gt_end, res_start, res_end)
            time = overlap_time(gt_start, gt_end, res_start, res_end)

            overlaps["overlaps"].append({"res_stop": res_entry, "score": score, "duration": time})

        stops_overlaps.append(overlaps)

    return stops_overlaps