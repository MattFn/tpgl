import math

def gaussian(x, height, center, width):
    return height * math.exp(-(x - center) * (x - center) / (2.0 * width * width))

def gaussianSmooth(data, n):
    res = []
    for i in range(len(data)):

        startIdx = max(0, i - n)
        endIdx = min(len(data) - 1, i + n)
        sumWeights = 0.0
        sumIndexWeight = 0.0

        for j in range(startIdx, endIdx+1):
            indexScore = float(abs(j - i)) / float(n)
            indexWeight = gaussian(indexScore, 1.0, 0.0, 1.0)
            sumWeights += indexWeight * data[j]
            sumIndexWeight += indexWeight

        res.append(sumWeights / sumIndexWeight)

    return res

def normalize(data):
    min_val = min(data)
    max_val = max(data)

    rang = max_val - min_val

    return [(x-min_val) / rang for x in data]

def findElbowIndex(data):
    bestIdx = 0
    bestScore = 0.0

    for i in range(len(data)):
        score = abs(data[i])
        if score > bestScore:
            bestScore = score
            bestIdx = i
        
    return bestIdx

def findElbowQuick(data):
    if len(data) <= 1:
        return 0
    else:
        normalisedData = normalize(gaussianSmooth(data, 3))

        for elbowIdx in range(len(normalisedData)):
            normalisedIndex = float(elbowIdx) / len(data)
            normalisedData[elbowIdx] -= normalisedIndex
        
        elbowIdx = findElbowIndex(normalisedData)
    return data[elbowIdx]
