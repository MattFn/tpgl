from __future__ import annotations

from ..managers import DataManager

from PyQt5.QtWidgets import QWidget, QLabel
from PyQt5.QtCore import pyqtSignal
from PyQt5.Qt import QVBoxLayout, QMargins

class StatusBar(QWidget):

    clicked = pyqtSignal()

    def __init__(self, data_manager:DataManager):
        super().__init__()
        self.data_manager = data_manager
        self.setLayout(QVBoxLayout())
        self.layout().setContentsMargins(QMargins(0, 0 ,0 ,0))

        self.label = QLabel("No file seleted ...")
        self.layout().addWidget(self.label)
        self.data_manager.data_changed.connect(self.update_filename)

    def update_filename(self):
        filename = self.data_manager.file_path
        if filename is None:
            filename = "No file seleted ..."

        self.label.setText(filename)

    def mousePressEvent(self, e):
        self.clicked.emit()