from PyQt5.QtWidgets import QSlider, QWidget, QLabel
from PyQt5.QtCore import Qt, pyqtSignal, QMargins
from PyQt5.Qt import QHBoxLayout

from qtrangeslider import QRangeSlider

USE_RANGE = True

class Slider(QWidget):

    point_index_change = pyqtSignal(int, int)
    
    def __init__(self, data_manager):
        super().__init__()
        self.data_manager = data_manager
        self.data_manager.data_changed.connect(self.update_range)
        
        if not USE_RANGE:
            self.slider = QSlider(self)
            self.slider.valueChanged.connect(self.on_slider_change)
        else:
            self.slider = QRangeSlider(self)
            self.slider.valueChanged.connect(self.on_rangeslider_change)

        self.slider.setMinimum(0)
        self.slider.setMaximum(0)
        self.slider.setOrientation(Qt.Horizontal)

        self.label_format = "{:5d}/{:5d}"
        self.label = QLabel(self.label_format.format(0, 0))

        self.setLayout(QHBoxLayout())
        self.layout().setContentsMargins(QMargins(0, 0, 0, 0))
        self.layout().addWidget(self.slider)
        self.layout().addWidget(self.label)


    def update_range(self):
        print("Slider: update_range")
        if not self.data_manager.is_data_loaded:
            return
            
        traj_len = self.data_manager.get_trajectory_data().get_length()

        self.slider.setMaximum(traj_len)
        self.set_value((0, traj_len), True)

        self.label_format = "{:" + str(5) + "d}/" + "{:" + str(5) + "d}"
        self.label.setText(self.label_format.format(traj_len, traj_len))

    def on_rangeslider_change(self, range):
        traj_len = self.data_manager.get_trajectory_data().get_length()
        
        self.point_index_change.emit(range[0], range[1])
        self.label.setText(self.label_format.format(self.slider.value()[1], traj_len))

    def on_slider_change(self, value):
        traj_len = self.data_manager.get_trajectory_data().get_length()
        
        self.point_index_change.emit(0, self.slider.value())
        self.label.setText(self.label_format.format(self.slider.value(), traj_len))

    def set_value(self, value, silent=False):
        if not USE_RANGE:
            value = value[1]

        if not silent:
            self.setValue(value)
            return

        self.slider.blockSignals(True)
        self.slider.setValue(value)
        self.slider.blockSignals(False)



