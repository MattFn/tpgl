import sys
import numpy as np

from PyQt5.QtWidgets import (QWidget, QDialog, QLineEdit, QGroupBox, QLabel, QPushButton, QColorDialog, QTabWidget, QScrollArea, QDoubleSpinBox, QProgressBar)
from PyQt5.Qt import QVBoxLayout, QColor, QGridLayout
from PyQt5.QtCore import Qt, pyqtSignal, QSize

#TODO remove DataManager and SettingsManager dependancy by passing colors to constructor
class LegendWidget(QWidget):

    MAX_CLASS_NUMBER = 100

    def __init__(self, data_manager, settings, editable=False):
        super().__init__()
        self.data_manager = data_manager
        self.settings = settings
        self.editable = editable
    
        self.data_manager.data_changed.connect(self.update_list)

        self.colors = None
        self.buttons = None

        self.setLayout(QVBoxLayout())

        if not self.editable:
            self.label = QLabel("class")
            self.layout().addWidget(self.label)

        self.scroll_area = QScrollArea()
        self.scroll_area_widget = QWidget()
        self.scroll_area_widget.setLayout(QGridLayout())

        self.scroll_area.setWidget(self.scroll_area_widget)
        self.layout().addWidget(self.scroll_area)

        self.update_list()

    # ugly as fuck, but I don't have time
    def update_list(self):
        self.settings_colors = self.settings.copy_colors() 
        if not self.data_manager.is_data_loaded:
            return
            
        if not self.editable:
            text = self.data_manager.get_current_annotation()
            if text is None: 
                text = "No class"
            self.label.setText(text)

        classes = self.data_manager.get_unique_classes()
        color_data = self.data_manager.get_color_data()

        current_colors = {a: self.settings_colors[color_data[a]] for a in classes}  #self.color_data #self.color_data[self.current_annotation]
    
        self.buttons = {}

        scroll_area_widget = QWidget()
        scroll_area_widget.setLayout(QGridLayout())

        excess = len(classes) - LegendWidget.MAX_CLASS_NUMBER
        l = LegendWidget.MAX_CLASS_NUMBER if excess >= 0 else len(classes)
        for i in range(l):
            c = QColor()
            c.setRgbF(*current_colors[classes[i]])

            if self.editable:
                w = QPushButton()
                w.clicked.connect(lambda e, idx=color_data[classes[i]] : self.open_color_picker(idx) )
                self.buttons[color_data[classes[i]]] = w 
            else:
                w = QLabel("      ")

            w.setStyleSheet('background-color: rgb({}, {}, {})'.format(c.red(), c.green(), c.blue()))
            scroll_area_widget.layout().addWidget(QLabel("{}".format(classes[i])), i, 1, 1, 1)
            scroll_area_widget.layout().addWidget(w, i, 0, 1, 1)
        
        if excess >= 0:
            scroll_area_widget.layout().addWidget(QLabel("{} more omitted...".format(excess)), i+1, 0, 1, 2)

        self.scroll_area.setWidget(scroll_area_widget)

    def open_color_picker(self, idx):
        color = QColorDialog.getColor()
        
        if color.isValid():
            self.settings_colors[idx] = color.getRgbF()
            self.buttons[idx].setStyleSheet('background: rgb({}, {}, {})'.format(color.red(), color.green(), color.blue()))
    
    def get_colors(self):
        return self.settings_colors
        
    def sizeHint(self):
        return QSize(150, 50)


