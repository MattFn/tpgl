from .legendwidget import LegendWidget
from .infowidget import InfoWidget
from .slider import Slider
from .statusbar import StatusBar