from PyQt5.QtWidgets import QWidget, QLabel, QListWidget
from PyQt5.Qt import QVBoxLayout, QMargins, QSize, QSizePolicy
from datetime import datetime

from ..TrajSeg.data import Point

class InfoWidget(QWidget):

    def __init__(self, plot:"GLWidget"):
        super().__init__()
        plot.ray_cast_result.connect(self.display)
        self.setLayout(QVBoxLayout())
        self.setMinimumSize(QSize(120, 50))
        self.setMaximumSize(QSize(500, 1000))
        
        self.list = QListWidget(self)
        self.list.setAlternatingRowColors(True)
        self.list.setStyleSheet("alternate-background-color: #dadada;")
        self.list.setContentsMargins(QMargins(0, 0, 0 ,0))
        self.list.addItem("No point selected...")

        self.layout().addWidget(self.list)

    def display(self, index, point:Point):
        self.clear_list()

        self.list.addItem("id:  {}".format(index))
        self.list.addItem("x:   {}".format(point.x))
        self.list.addItem("y:   {}".format(point.y))
        self.list.addItem("ts:  {}".format(str(point.timestamp)))
        for k, v in point.annotations.items():
            self.list.addItem("{}:  {}".format(k, v))


    def clear_list(self):
        self.list.clear()
    
    def sizeHint(self):
        return QSize(150, 50)