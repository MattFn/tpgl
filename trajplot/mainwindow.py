import sys
import os
import numpy as np
import json
import datetime as dt
from os import path, makedirs
from csv import DictReader, writer

import time

from PyQt5.QtWidgets import (QWidget, QMainWindow, QAction, QApplication, QSlider, QFileDialog, QMessageBox, QDockWidget)
from PyQt5.Qt import QVBoxLayout
from PyQt5.QtGui import QSurfaceFormat, QIcon, QPixmap
from PyQt5.QtCore import Qt, pyqtSignal

from .menu import Menu
from .utils import QtUtils
from .managers import SettingsManager, DataManager
from .data import SettingsObject, TrajectoryData
from .dialogs import OpenFileDialog, SettingsDialog, SegmentationDialog, EvaluateDialog
from .widgets import LegendWidget, InfoWidget, Slider, StatusBar

from .plotwidget import GLWidget

class TrajPlotGL(QMainWindow):

    SETTINGS_FOLDER = path.expanduser('~\\TrajPlotGL')
    EXPORTS_FOLDER = path.expanduser('~\\TrajPlotGL\\exports')
    SETTINGS_PATH = SETTINGS_FOLDER + "\\settings.json"

    def __init__(self, *args, **kwargs):
        super(TrajPlotGL, self).__init__(*args, **kwargs)
        self.w_size = (1000, 800)
        self.s_size = QApplication.desktop().screenGeometry().width(), QApplication.desktop().screenGeometry().height()

        self.resize(self.w_size[0], self.w_size[1])
        self.move(int(self.s_size[0]/2 - self.w_size[0]/2), int(self.s_size[1]/2 - self.w_size[1]/2))
        self.setWindowTitle("TrajPlotGL")

        # general variables
        self.last_path = None
        self.application_path = sys._MEIPASS if getattr(sys, 'frozen', False) else sys.path[0]
        
        # bottom bar
        self.statusbar = self.statusBar()
        
        # init settings
        self.settings_manager = SettingsManager(TrajPlotGL.SETTINGS_PATH)

        # init data
        self.data_manager = DataManager(self.settings_manager)

        # slider
        self.slider = Slider(self.data_manager)

        # init plot
        self.plot_widget = GLWidget(self, self.data_manager, self.settings_manager, self.slider)
    
        # Set Menu
        self.setMenuBar(Menu(self, self))

        # legend
        self.legend_dock = QDockWidget("Legend")
        self.legend_dock.setAllowedAreas(Qt.RightDockWidgetArea)
        self.legend_dock.setWidget(LegendWidget(self.data_manager, self.settings_manager)) 
        
        # info
        self.info_dock = QDockWidget("Info")
        self.info_dock.setAllowedAreas(Qt.RightDockWidgetArea)
        self.info_dock.setWidget(InfoWidget(self.plot_widget))

        # status bar
        self.status = StatusBar(self.data_manager)
        self.status.clicked.connect(self.open_file)
        
        # central widget
        central_widget = QWidget(self)
        central_widget.setLayout(QVBoxLayout())
        
        # mid container for plot and docks
        mid = QMainWindow(central_widget)
        mid.setWindowFlags(Qt.Widget)
        mid.setCentralWidget(self.plot_widget)
        mid.addDockWidget(Qt.RightDockWidgetArea, self.legend_dock)
        mid.addDockWidget(Qt.RightDockWidgetArea, self.info_dock)

        central_widget.layout().addWidget(self.status)
        central_widget.layout().addWidget(mid)
        central_widget.layout().addWidget(self.slider)

        self.setCentralWidget(central_widget)

        self.init_done = True
      
    def on_slider_change(self):
        self.index_changed.emit(self.slider.value())

    def on_computation_done(self, result):
        self.data_manager.set_cl_output(result)
        self.data_manager.activate_cl_output(True)
        self.reload_plot.emit()  

    # Menu handlers
    def quit(self):
        QApplication.quit()

    def open_file(self):
        dialog = OpenFileDialog(self, self.data_manager, self.settings_manager)
        dialog.exec()

    def open_settings(self):
        print("Main Window: open_settings")
        dialog = SettingsDialog(self, self.settings_manager, self.data_manager)
        dialog.exec()

    def open_segmentation(self):
        print("Main Window: open_segmentation")
        dialog = SegmentationDialog(self, self.data_manager, self.settings_manager)
        if not self.data_manager.is_data_loaded:
            QtUtils.display_message("No data", "Warning: to apply an algorithm you must first load some data.", 1)
            return

        dialog.exec()

    def toggle_info(self):
        print("Main Window: toggle_Info")
        if self.info_dock.isVisible():
            self.info_dock.hide()
        else:
            self.info_dock.show()

    def toggle_legend(self):
        print("Main Window: toggle_Legend")
        if self.legend_dock.isVisible():
            self.legend_dock.hide()
        else:
            self.legend_dock.show()

    def cycle_annotations(self):
        if not self.data_manager.is_data_loaded:
            return
            
        self.data_manager.cycle_annotations()

    def toggle_axis(self):
        self.plot_widget.toggle_axis()

    def toggle_labels(self):
        self.plot_widget.toggle_labels()

    def toggle_ticks(self):
        self.plot_widget.toggle_ticks()

    def export_CSV(self):
        if not self.data_manager.is_data_loaded:
            QtUtils.display_message("Warning", "No file to export", mode=2)
            return
            
        makedirs(TrajPlotGL.EXPORTS_FOLDER, exist_ok=True)
        output_file = QtUtils.create_output_filename(TrajPlotGL.EXPORTS_FOLDER, self.data_manager.file_name, ".csv")

        self.data_manager.to_csv(output_file)
        QtUtils.display_message("CSV export complete", "exported to:\n{}".format(TrajPlotGL.EXPORTS_FOLDER), 2)

    def export_PNG(self):
        if not self.data_manager.is_data_loaded:
            QtUtils.display_message("Warning", "No file to export", mode=2)
            return

        makedirs(TrajPlotGL.EXPORTS_FOLDER, exist_ok=True)
        img = self.plot_widget.grab()

        output_file = QtUtils.create_output_filename(TrajPlotGL.EXPORTS_FOLDER, self.data_manager.file_name, ".png")

        img.save(output_file)

        QtUtils.display_message("PNG export complete", "exported to:\n{}".format(TrajPlotGL.EXPORTS_FOLDER), 2)

    def evaluate(self):
        if not self.data_manager.is_data_loaded:
            QtUtils.display_message("No data", "Warning: to evaluate a result you must first load some data.", 1)
            return
            
        dialog = EvaluateDialog(self, self.data_manager, TrajPlotGL.EXPORTS_FOLDER)
        dialog.exec()