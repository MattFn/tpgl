# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['main.py'],
             pathex=['C:/Users/fenum/Desktop/TrajPlotGLRefactor/trajplotgl'],
             binaries=[],
             datas=[
                 ('trajplot/resources/trajploticon.ico', 'trajplot/resources'), 
                 ('trajplot/resources/font/Verdana112.png', 'trajplot/resources/font'),
                 ('trajplot/resources/font/Verdana112.fnt', 'trajplot/resources/font')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          a.binaries,
          a.zipfiles,
          a.datas,
          name='TrajPlotGL',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True,
          icon='./trajplot/resources/trajploticon.ico'
          )
"""
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='main')
"""